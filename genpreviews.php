#!/usr/bin/env php
<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(PHP_SAPI!=='cli'){die('<br />Commandline only!');}
// Launch in BG with system('php genpreviews.php > /dev/null &')
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
// Make sure only one instance is running at a time
$lock=fopen('files', 'r');
if(!flock($lock, LOCK_EX|LOCK_NB)){die("Lock failed");}
include_once('db.php');
include_once('config.php');
include_once('files.php');
$regen=($argv[1]=='all'); // Regenerate all previews or only those missing 2D previews
@mkdir(TMPDIR, 0755);
$res=mysqli_query($db, 'select distinct lower(substring_index(name,".",-1)) as type, hash from files');
while($row=mysqli_fetch_assoc($res))
{
  $file=getfilepath($row['hash'], true);
  // Preview image
  if(file_exists($file.'.png') && !$regen){continue;}
  $img=false;
  switch($row['type'])
  {
    // Scale down images to more suitable sizes
    case 'png': if(!$img){$img=imagecreatefrompng($file);}
    case 'jpg':
    case 'jpeg': if(!$img){$img=imagecreatefromjpeg($file);}
    case 'gif': if(!$img){$img=imagecreatefromgif($file);}
      if(!$img){break;} // Failed to load image
      $x=imagesx($img);
      $y=imagesy($img);
      if($x/PREVIEW_SIZE[0]<$y/PREVIEW_SIZE[1]) // Maintain aspect ratio
      {
        $ox=$x*PREVIEW_SIZE[1]/$y;
        $oy=PREVIEW_SIZE[1];
      }else{
        $ox=PREVIEW_SIZE[0];
        $oy=$y*PREVIEW_SIZE[0]/$x;
      }
      $out=imagecreatetruecolor($ox, $oy);
      imagecopyresampled($out, $img, 0, 0, 0, 0, $ox, $oy, $x, $y);
      imagepng($out, $file.'.png');
      imagedestroy($img);
      imagedestroy($out);
      break;
    case 'blend':
    case '3mf':
    case 'x3d':
    case 'obj':
    case 'stl':
      // Convert to STL for the next step (even for stl for bin->ascii)
      copy($file, TMPDIR.'/thingsharepreview_orig.'.$row['type']);
      system('assimp export '.escapeshellarg(TMPDIR.'/thingsharepreview_orig.'.$row['type']).' '.escapeshellarg(TMPDIR.'/thingsharepreview.stl'));
      // Convert to X3D for 3D view while we're at it
      system('assimp export '.escapeshellarg(TMPDIR.'/thingsharepreview_orig.'.$row['type']).' '.escapeshellarg(TMPDIR.'/thingsharepreview.x3d'));
      unlink(TMPDIR.'/thingsharepreview_orig.'.$row['type']);
      $min=false;
      $max=false;
      switch(PREVIEW_RENDERMETHOD)
      {
        case 'povray':
          // Translate STL to Povray (could have used stl2pov for this but it required a lot of tweaking of the output and didn't give us access to useful data like min/max vertex positions)
          $min=Array(false,false,false);
          $max=Array(false,false,false);
          $in=fopen(TMPDIR.'/thingsharepreview.stl', 'r');
          $out=fopen(TMPDIR.'/thingsharepreview.pov', 'w');
          while($line=fgets($in))
          {
            if(substr(trim($line), 0, 7)=='vertex ')
            {
              $vertex=array_slice(explode(' ', trim($line)), 1);
              $facet[]=$vertex;
              for($i=0; $i<3; ++$i)
              {
                if($min[$i]===false || $vertex[$i]<$min[$i]){$min[$i]=$vertex[$i];}
                if($max[$i]===false || $vertex[$i]>$max[$i]){$max[$i]=$vertex[$i];}
              }
            }
            elseif(substr(trim($line), 0, 6)=='facet ')
            {
              $facet=Array(); // Start empty
            }
            elseif(trim($line)=='endfacet')
            {
              if(count($facet)==3){fwrite($out, "  triangle {\n");}
              elseif(count($facet)==4){fwrite($out, "  square {\n");}
              foreach($facet as $vertex)
              {
                fwrite($out, '    <'.$vertex[0].','.$vertex[1].','.$vertex[2].">\n");
              }
              fwrite($out, "  }\n");
            }
            elseif(substr(trim($line), 0, 6)=='solid ')
            {
              fwrite($out, "mesh {\n");
            }
          }
          fclose($in);
          $dist=0;
          $maxdist=0;
          $avg=Array();
          for($i=0; $i<3; ++$i)
          {
            $avg[$i]=($max[$i]+$min[$i])/2;
            if($max[$i]-$min[$i]>$maxdist){$maxdist=$max[$i]-$min[$i];}
            $dist+=($max[$i]-$min[$i])/3;
          }
          $dist=($dist+$maxdist)/2; // $dist and $maxdist worked great for different cases, averaging gives us decent enough results for all cases. TODO: An even more accurate method would likely be to convert each vertex coordinate to be in relation to the camera
          fwrite($out, "  texture {\n");
          fwrite($out, '    pigment { color rgb<'.(PREVIEW_COLOR[0]/255).','.(PREVIEW_COLOR[1]/255).','.(PREVIEW_COLOR[2]/255)."> }\n");
          fwrite($out, "    finish { ambient 0.2 diffuse 0.7 }\n");
          fwrite($out, "  }\n}\n");
          // Scene setup
          fwrite($out, "global_settings { ambient_light rgb<1, 1, 1> }\n");
          fwrite($out, "background { color rgb<1,1,1> }\n"); // TODO: Configurable background color?
          fwrite($out, "camera {\n");
          $dist/=1.65;
          fwrite($out, '  location <'.($max[0]+$dist).','.($max[1]+$dist).','.($max[2]+$dist).">\n");
          fwrite($out, '  look_at <'.$avg[0].','.$avg[1].','.$avg[2].">\n");
          fwrite($out, '  right -x*'.(PREVIEW_SIZE[0]/PREVIEW_SIZE[1])."\n");
          fwrite($out, "  sky <0,0,1>\n}\n"); // Set Z vertical
          fwrite($out, 'light_source { <'.($max[0]+$dist+10).','.($max[1]+$dist).','.($max[2]+$dist+20).'> color rgb<1, 1, 1> }'); // Light slightly offset from camera to distinguish corners better
          fclose($out);
          // Render using Povray
          $tmpfile=escapeshellarg(TMPDIR.'/thingsharepreview.pov');
          system('povray +I'.$tmpfile.' +O'.escapeshellarg($file.'.png').' -D +P +W'.(int)PREVIEW_SIZE[0].' +H'.(int)PREVIEW_SIZE[1].' +A0.5');
          unlink(TMPDIR.'/thingsharepreview.pov');
          break;
        case 'openscad': // TODO: Test this code (not well tested so far)
          $c=sprintf('#%02x%02x%02x', PREVIEW_COLOR[0], PREVIEW_COLOR[1], PREVIEW_COLOR[2]);
          file_put_contents(TMPDIR.'/thingsharepreview.scad', 'color("'.$c.'")import("thingsharepreview.stl");');
          if(getenv('DISPLAY')==''){putenv('DISPLAY=:0');} // Default X server
          system('openscad --imgsize '.(int)PREVIEW_SIZE[0].','.(int)PREVIEW_SIZE[1].' -o '.escapeshellarg($file.'.png').' '.escapeshellarg(TMPDIR.'/thingsharepreview.scad'));
          unlink(TMPDIR.'/thingsharepreview.scad');
          break;
      }
      if(!$min || !$max) // If we haven't found them as part of 2D rendering, find minmax coordinates
      {
        $in=fopen(TMPDIR.'/thingsharepreview.stl', 'r');
        while($line=fgets($in))
        {
          if(substr(trim($line), 0, 7)=='vertex ')
          {
            $vertex=array_slice(explode(' ', trim($line)), 1);
            for($i=0; $i<3; ++$i)
            {
              if($min[$i]===false || $vertex[$i]<$min[$i]){$min[$i]=$vertex[$i];}
              if($max[$i]===false || $vertex[$i]>$max[$i]){$max[$i]=$vertex[$i];}
            }
          }
        }
        fclose($in);
      }
      $dist=0;
      $maxdist=0;
      $avg=Array();
      for($i=0; $i<3; ++$i)
      {
        $avg[$i]=($max[$i]+$min[$i])/2;
        if($max[$i]-$min[$i]>$maxdist){$maxdist=$max[$i]-$min[$i];}
        $dist+=($max[$i]-$min[$i])/3;
      }
      $dist=($dist+$maxdist)/3.3;
      // Edit X3D to match
      $in=fopen(TMPDIR.'/thingsharepreview.x3d', 'r');
      $out=fopen($file.'.x3d', 'w');
      $c=sprintf('#%02x%02x%02x', PREVIEW_COLOR[0]*0.75, PREVIEW_COLOR[1]*0.75, PREVIEW_COLOR[2]*0.75);
      $cs=sprintf('#%02x%02x%02x', PREVIEW_COLOR[0], PREVIEW_COLOR[1], PREVIEW_COLOR[2]);
      while($line=fgets($in, 2048))
      {
        // Replace appearance with our own
        if(substr_count(strtolower($line), '<appearance ')>0)
        {
          while(substr_count(strtolower($line), '</appearance>')<1 && ($line=fgets($in)));
          $line='<appearance><material diffusecolor="'.$c.'" specularcolor="'.$cs.'"></material></appearance>';
        }
        if(substr_count(strtolower($line), '<scene>')>0 && substr_count(strtolower($line), '<!--')==0)
        {
          // Calculate orientation
          $x=$avg[0]-($max[0]+$dist);
          $y=$avg[1]-($max[1]+$dist);
          $z=$avg[2]-($max[2]+$dist);
          $rz=-atan2($x, $y);
          $xy=hypot($x*cos($rz), $y*sin($rz));
          if($x<0 && $y<0){$xy=-$xy;} // Restore sign of values turned absolute in the process
          $xy*=1.5; // Not sure why, but without this view ends up too low
          $rx=atan2($xy, $z)+pi();
          $line.='<transform translation="'.($max[0]+$dist).','.($max[1]+$dist).','.($max[2]+$dist).'">
<transform rotation="0,0,1,'.$rz.'">
<transform rotation="1,0,0,'.$rx.'">
<viewpoint></viewpoint>
</transform>
</transform>
</transform>'."\n";
        }
        fwrite($out, $line);
      }
      fclose($in);
      fclose($out);
      // Clean up
      unlink(TMPDIR.'/thingsharepreview.x3d');
      unlink(TMPDIR.'/thingsharepreview.stl');
      break;
  }
//  print($file.'.'.$row['type']."\n");
}
?>
