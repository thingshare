<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(!isset($path[3])){die();}
include_once('db.php');
include_once('files.php');
$id=(int)$path[3];
$res=mysqli_query($db, 'select id, name, description, user, posted, license from things where thingid='.$id.' and latest and !removed');
$row=mysqli_fetch_assoc($res);
if(!$row){header('HTTP/1.1 404 Not found'); die('{"error":"Thing not found"}');}
$thing=Array('id'=>$id,
             'name'=>$row['name'],
             'description'=>$row['description'],
             'date'=>$row['posted'],
             'files'=>Array(),
             'license'=>Array('name'=>$row['license']));
$user=(int)$row['user'];
$revid=(int)$row['id'];
// List files
$res=mysqli_query($db, 'select name, hash from files where thing='.(int)$row['id']);
while($row=mysqli_fetch_assoc($res))
{
  $file=Array('name'=>$row['name'],
              'path'=>getfilepath($row['hash']),
              'preview'=>getpreview($row['name'], $row['hash']));
  if($preview3d=get3dpreview($row['name'], $row['hash'])){$file['preview3d']=$preview3d;}
  // Grab mimetype, if set
  $mime=db_getmimetype($row['name']);
  if($mime!==false){$file['type']=$mime;}
  $thing['files'][]=$file; // Add to list
}
// Designer
$res=mysqli_query($db, 'select displayname, name from users where id='.$user);
$row=mysqli_fetch_assoc($res);
$thing['by']=$row;
// Look up license's simplified description
if($thing['license']['name']!='other')
{
  $name=mysqli_real_escape_string($db, $thing['license']['name']);
  $res=mysqli_query($db, 'select simple from licenses where name="'.$name.'"');
  if($row=mysqli_fetch_row($res)){$thing['license']['simple']=$row[0];}
}
// Tags
$thing['tags']=Array();
$res=mysqli_query($db, 'select tags.name from tags, tagmaps where tags.id=tagmaps.tag and tagmaps.thing='.$revid);
while($row=mysqli_fetch_row($res))
{
  $thing['tags'][]=$row[0];
}
print(json_encode($thing));
?>
