<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
if(isset($_COOKIE['PHPSESSID'])){session_start();}
if(!isset($_SESSION['id'])){header('Location: '.BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']));}
include_once('db.php');
include_once('nonce.php');
if(isset($_POST['displayname']) && isset($_POST['profile']) && checknonce())
{
  // Save and redirect back to profile
  $displayname=mysqli_real_escape_string($db, $_POST['displayname']);
  $profile=mysqli_real_escape_string($db, $_POST['profile']);
  mysqli_query($db, 'update users set displayname="'.$displayname.'", profile="'.$profile.'" where id='.(int)$_SESSION['id']);
  header('Location: '.BASEURL.'/user/'.$_SESSION['name'].'@'.DOMAIN);
}
include_once('head.php');
$res=mysqli_query($db, 'select displayname, profile from users where id='.(int)$_SESSION['id']);
$res=mysqli_fetch_assoc($res);
// TODO: Profile picture?
?>
<script src="<?=BASEURL?>/mdjs/mdjs.js"></script>
<form method="post">
  <?=nonce()?>
  <a href="<?=BASEURL?>/changepassword"><?=_('Change password')?></a><br />
  <?=_('Display name:')?> <input type="text" name="displayname" value="<?=htmlentities($res['displayname'])?>" /><br />
  <?=_('Profile:')?><br />
  <textarea name="profile" rows="15" style="width:100%;" onchange="document.getElementById('mdpreview').innerHTML='Markdown preview:<br />'+Mdjs.md2html(this.value.replace(/&/g,'&amp;amp;').replace(/</g,'&amp;lt;'));" onkeyup="this.onchange();"><?=htmlentities($res['profile'])?></textarea><br />
  <div id="mdpreview"></div>
  <button><?=_('Save')?></button>
</form>
