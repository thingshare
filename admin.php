<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(isset($_COOKIE['PHPSESSID'])){session_start();}
if(!isset($_SESSION['id'])){header('Location: '.BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']));}
include_once('head.php');
switch($path[2])
{
  case 'privileges': include('admin_privileges.php'); break;
  case 'moderate': include('admin_moderate.php'); break;
  case 'federation': include('admin_federation.php'); break;
  case 'filetypes': include('admin_filetypes.php'); break;
  case 'licenses': include('admin_licenses.php'); break;
  case 'tags': include('admin_tags.php'); break;
  default:
    if($privileges&PRIV_PRIVILEGES){print('<a href="'.BASEURL.'/admin/privileges">Manage user privileges</a><br />');}
    if($privileges&PRIV_MODERATE){print('<a href="'.BASEURL.'/admin/moderate">Moderation options</a><br />');}
    if($privileges&PRIV_FEDERATION){print('<a href="'.BASEURL.'/admin/federation">Manage peer federation</a><br />');}
    if($privileges&PRIV_FILETYPES){print('<a href="'.BASEURL.'/admin/filetypes">Manage filetypes</a><br />');}
    if($privileges&PRIV_LICENSES){print('<a href="'.BASEURL.'/admin/licenses">Manage the license list</a><br />');}
    if($privileges&PRIV_TAGS){print('<a href="'.BASEURL.'/admin/tags">Manage tags</a><br />');}
}
?>
