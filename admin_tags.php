<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
include_once('rpc.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_TAGS)){die(_('Insufficient privileges'));}

$error='';
if(checknonce()) // Save changes
{
  if(isset($_POST['options']))
  {
    setoption('usertags', isset($_POST['usertags']));
  }
  if(isset($_POST['addtag']) || isset($_POST['blacklisttag']))
  {
    $tag=mysqli_real_escape_string($db, $_POST['newtag']);
    $blacklist='0';
    if(isset($_POST['blacklisttag']))
    {
      $blacklist='1';
      $res=mysqli_query($db, 'select id from tags where name="'.$tag.'"');
      if($res=mysqli_fetch_row($res))
      {
        mysqli_query($db, 'delete from tagmaps where tag="'.(int)$res[0].'"');
      }
    }
    mysqli_query($db, 'delete from tags where name="'.$tag.'"');
    mysqli_query($db, 'insert into tags(name, blacklist, optin) values("'.$tag.'", '.$blacklist.', "")');
  }
  if(isset($_POST['deletetag']))
  {
    $tag=(int)$_POST['deletetag'];
    mysqli_query($db, 'delete from tagmaps where tag="'.$tag.'"');
    mysqli_query($db, 'delete from tags where id="'.$tag.'"');
  }
  if(isset($_POST['tag_optin']))
  {
    $id=(int)$path[3];
    $optin=mysqli_real_escape_string($db, $_POST['tag_optin']);
    mysqli_query($db, 'update tags set optin="'.$optin.'" where id='.$id);
    header('Location: '.BASEURL.'/admin/tags');
  }
}
if($error!=''){$error='<span class="error">'.$error.'</span>';}

// Load current
if(!isset($path[3]))
{
  $tags='';
  $res=mysqli_query($db, 'select name, blacklist, id from tags order by name asc');
  while($row=mysqli_fetch_assoc($res))
  {
    $name=htmlentities($row['name']);
    if($row['blacklist']){$name='<span class="blacklist">'.$name.'</span>';}
    $tags.='<a href="'.BASEURL.'/admin/tags/'.$row['id'].'">'.$name.'</a> <button name="deletetag" value="'.$row['id'].'">X</button><br />';
  }
  $usertagscheck=(getoption('usertags', true)?' checked':'');
}else{ // Individual tag
  $id=(int)$path[3];
  $res=mysqli_query($db, 'select name, blacklist, optin from tags where id='.$id);
  $res=mysqli_fetch_assoc($res);
  $name=$res['name'];
  if($res['blacklist'])
  {
    $optin='';
    $placeholder='"'._('Blacklisted').'" disabled';
  }else{
    $optin=htmlentities($res['optin']);
    $placeholder='"'._('No opt-in required').'"';
  }
  $res=mysqli_query($db, 'select count(*) from tagmaps where tag='.$id);
  $numtags=mysqli_fetch_row($res)[0];
}
if(!isset($path[3]))
{
?>
<?=$error?>
<form method="post"><?=nonce()?>
  <h2><?=_('Options')?></h2>
  <label><input type="checkbox" name="usertags"<?=$usertagscheck?> /><?=_('Allow users to create tags')?></label><br />
  <input type="submit" name="options" value="<?=_('Save')?>" />
</form>
<form method="post"><?=nonce()?>
<h2><?=_('Tags')?></h2>
<?=$tags?>
  <input type="text" name="newtag" /><button name="addtag"><?=_('Add tag')?></button><button name="blacklisttag" title="<?=_('Don\'t allow this tag to be added')?>"><?=_('Add tag to blacklist')?></button><br />
</form>
<?php }else{ /* Individual tag */ ?>
<h2>Tag '<?=$name?>'</h2>
<p><?=sprintf(_('%s things tagged on this node.'), $numtags)?></p>
<form method="post"><?=nonce()?>
  <?=_('Opt-in text:')?><br />
  <textarea name="tag_optin" rows="12" style="width:100%;" placeholder=<?=$placeholder?>><?=$optin?></textarea><br />
  <input type="submit" value="<?=_('Save')?>" />
</form>
<a href="<?=BASEURL?>/admin/tags"><button><?=_('Back')?></button></a>
<?php } ?>
