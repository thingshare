<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
$menu=''; // Additional menu items depending on privileges/just being logged in
$unreadmsgs='';
$notifications='';
$notifications_new=false;
function timeago($time)
{
  $time=time()-strtotime($time);
  $unit='';
  if($time>3600*24*365.25)
  {
    $time=round($time/(3600*24*365.25));
    $unit='year';
  }
  elseif($time>3600*24*7)
  {
    $time=round($time/(3600*24*7));
    $unit='week';
  }
  elseif($time>3600*47)
  {
    $time=round($time/(3600*24));
    $unit='day';
  }
  elseif($time>3600)
  {
    $time=round($time/3600);
    $unit='hour';
  }
  elseif($time>60)
  {
    $time=round($time/60);
    $unit='minute';
  }
  if($unit!='')
  {
    // TODO: Use gettext pluralization
    return $time.' '.$unit.($time==1?'':'s').' ago';
  }
  return 'just now';
}
if(isset($_COOKIE['PHPSESSID'])) // TODO: See if there's a better way to check if there is a session to resume
{
  session_start();
  if(isset($_SESSION['id']))
  {
    include_once('db.php');
    $res=mysqli_query($db, 'select privileges, status from users where id='.(int)$_SESSION['id']);
    $res=mysqli_fetch_row($res);
    if($res[1]==ACCOUNT_BANNED)
    {
      print('Banned'); // TODO: Put this nicely?
      session_destroy();
      exit();
    }
    $privileges=$res[0];
    if($privileges>0){$menu.='<a href="'.BASEURL.'/admin">Administration</a>';}
    // Check for unread messages
    $res=mysqli_query($db, 'select id from messages where user='.(int)$_SESSION['id'].' and !msgread limit 1');
    if(mysqli_num_rows($res)>0){$unreadmsgs=' class="highlight"';}
    // Check for notifications
    if(isset($_GET['notification']))
    {
      // TODO: Is there a cleaner way to update 'seen' than passing the ID in GET?
      mysqli_query($db, 'update notifications set seen=true where user='.(int)$_SESSION['id'].' and id='.(int)$_GET['notification']);
    }
    $res=mysqli_query($db, 'select id, message, link, sent, seen from notifications where user='.(int)$_SESSION['id'].' order by sent desc limit 20');
    while($row=mysqli_fetch_assoc($res))
    {
      $link=$row['link'];
      if(!$row['seen'])
      {
        $notifications_new=true;
        if($pos=strpos($link, '#'))
        {
          $link=substr($link,0,$pos).'?notification='.$row['id'].substr($link,$pos);
        }else{
          $link.='?notification='.$row['id'];
        }
      }
      $notifications.=timeago($row['sent']).' <a href="'.BASEURL.$link.'"><div class="notification'.($row['seen']?'':' notification-unseen').'">'.$row['message'].'</div></a><br />';
    }
  }
}
$loginlink=BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']);
$logoutlink=BASEURL.'/logout?returnto='.urlencode($_SERVER['REQUEST_URI']);
if($path[1]=='tag')
{
  $search='tag:'.htmlentities($path[2]);
}else{
  $search=htmlentities(isset($_GET['q'])?$_GET['q']:'');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=NODENAME?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?=BASEURL?>/style.css" type="text/css" />
  <link rel="stylesheet" href="<?=BASEURL?>/x3dom.css" type="text/css" />
  <script src="<?=BASEURL?>/x3dom.debug.js"></script>
  <script src="<?=BASEURL?>/3dview.js"></script>
  <script src="<?=BASEURL?>/web2.0.js"></script>
</head>
<body>
  <div id="logo" title="Yes this needs a logo or something. Got any art skills?"></div>
<!-- TODO: Responsive design -->
  <nav>
    <img src="<?=BASEURL?>/icons/menu.svg" width="32" height="32" onerror="this.src='<?=BASEURL?>/icons/menu.png';" id="menuicon" />
    <div id="menu">
      <a href="<?=BASEURL?>/browse/new">Latest designs</a>
      <?=$menu?>
    </div>
    <div id="user"><?php if(isset($_SESSION['name'])){ ?>
      <div class="dropdown-container">
        <div class="dropdown">
          <img src="<?=BASEURL?>/icons/bell.svg" width="32" height="32" onerror="this.src='<?=BASEURL?>/icons/bell.png';" style="display:block; opacity:<?=($notifications_new?1:0.5)?>;" />
          <div class="dropdown-content">
            <?=$notifications?>
          </div>
        </div>
      </div>
      <!-- Link to user settings, messages -->
      <a href="<?=BASEURL?>/user/<?=htmlentities($_SESSION['name'].'@'.DOMAIN)?>" id="profilelink"><?=$_SESSION['displayname']?></a>
      <a href="<?=BASEURL?>/messages"<?=$unreadmsgs?>>Messages</a>
      <a href="<?=$logoutlink?>">Log out</a>
    <?php }else{ ?>
      <a href="<?=$loginlink?>">Log in</a>
      <a href="<?=BASEURL?>/register">Register</a>
    <?php } ?>
    </div>
    <div id="search">
      <form method="get" action="<?=BASEURL?>/search">
        <input type="text" name="q" value="<?=$search?>" placeholder="Search" />
        <button><img src="<?=BASEURL?>/icons/find.svg" onerror="this.src='<?=BASEURL?>/icons/find.png';" /></button>
      </form>
    </div>
  </nav>
  <div id="content">
