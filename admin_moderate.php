<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_MODERATE)){die(_('Insufficient privileges'));}

// Report management
if(isset($_POST['report']) && isset($_POST['action']) && checknonce())
{
  $report=(int)$_POST['report'];
  $res=mysqli_query($db, 'select target, reason from reports where id='.$report);
  $row=mysqli_fetch_assoc($res);
  if(!$row){die('<div class="error"><h1>Report not found</h1></div>');}
  $target=$row['target'];
  $splittarget=explode('/', $target);
  $reason=$row['reason'];
  $action='';
  switch($_POST['action'])
  {
    case 'deletething':
      if(substr($target,0,6)!='thing/' || !is_numeric(substr($target,6))){die('<div class="error"><h1>Not a thing</h1></div>');}
      $id=(int)substr($target,6);
      // Non-destructive removal
      mysqli_query($db, 'update things set removed=true where thingid='.$id);
      $action='deleted thing';
      break;
    case 'deletecomment':
      if($splittarget[0]!='comment' || !is_numeric($splittarget[1]) || !is_numeric($splittarget[2])){die('<div class="error"><h1>Not a comment</h1></div>');}
      $id=(int)$splittarget[2];
      // Non-destructive removal
      mysqli_query($db, 'update comments set removed=true where id='.$id);
      $action='deleted comment';
      break;
    case 'ban':
      // Track down user
      if(substr($target,0,6)!='thing/' || !is_numeric(substr($target,6))){die('<div class="error"><h1>Not a thing</h1></div>');} // TODO: Handle comments
      // The problem with comments is commenters can be from other nodes and we can't ban on other nodes. maybe we need a remotebans table? In the meantime user-level blocking might be enough
      $id=(int)substr($target,6);
      $res=mysqli_query($db, 'select user from things where id='.$id);
      $user=(int)mysqli_fetch_row($res)[0];
      mysqli_query($db, 'update users set status='.ACCOUNT_BANNED.' where id='.$user);
      $action='banned user <a href="'.BASEURL.'/user/'.db_getuser($user).'@'.DOMAIN.'">'.db_getuser($user).'</a>';
      break;
    case 'deletereport':
// TODO: Modlog report deletion too? seems excessive but could be useful
      mysqli_query($db, 'delete from reports where id='.$report);
      break;
  }
  if($action!='') // Log action
  {
    $action=mysqli_real_escape_string($db, $action.' for report "'.$reason.'"');
    $timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
    $target=mysqli_real_escape_string($db, $target);
    mysqli_query($db, 'insert into moderationlog(user, timestamp, action, target) values('.(int)$_SESSION['id'].', "'.$timestamp.'", "'.$action.'", "'.$target.'")');
  }
}
if(isset($_POST['unban']) && checknonce())
{
  $user=mysqli_real_escape_string($db, $_POST['unban']);
  $timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
  mysqli_query($db, 'update users set status='.ACCOUNT_ACTIVE.' where name="'.$user.'"');
  mysqli_query($db, 'insert into moderationlog(user, timestamp, action, target) values('.(int)$_SESSION['id'].', "'.$timestamp.'", "unbanned user", "user/'.$user.'")');
}

// Gather moderation log entries
$modlog='';
$res=mysqli_query($db, 'select user, timestamp, action, target from moderationlog order by timestamp desc limit 20'); // TODO: Paging?
while($row=mysqli_fetch_assoc($res))
{
// TODO: Figure out timezones
  $linksplit=explode('/', $row['target']);
  if($linksplit[0]=='comment')
  {
    $link=htmlentities('thing/'.$linksplit[1].'@'.DOMAIN.'#comment'.$linksplit[2]);
  }else{
    $link=htmlentities(implode('/', $linksplit).'@'.DOMAIN);
  }
  $modlog.=$row['timestamp'].' <a href="'.BASEURL.'/user/'.db_getuser($row['user']).'@'.DOMAIN.'">'.db_getuser($row['user']).'</a> '.$row['action'].' on <a href="'.BASEURL.'/'.$link.'">'.$row['target'].'</a><br />';
}
if(mysqli_num_rows($res)==20){$modlog.='TODO: Paging?<br />';}

// Gather reports
$reports='';
$res=mysqli_query($db, 'select id, user, target, reason, timestamp from reports order by timestamp');
while($row=mysqli_fetch_assoc($res))
{
  $id=$row['id'];
  $user=htmlentities($row['user']);
  $subject=htmlentities($row['target']);
  $linksplit=explode('/', $row['target']);
  if($linksplit[0]=='comment')
  {
    $link=htmlentities('thing/'.$linksplit[1].'@'.DOMAIN.'#comment'.$linksplit[2]);
  }else{
    $link=htmlentities(implode('/', $linksplit).'@'.DOMAIN);
  }
  $reason=htmlentities($row['reason']);
  $timestamp=htmlentities($row['timestamp']);
  $reports.='<tr><td><a href="'.BASEURL.'/user/'.$user.'">'.$user.'</a></td>';
  $reports.='<td><a href="'.BASEURL.'/'.$link.'">'.$subject.'</a></td>';
  $reports.='<td>'.$reason.'</td>';
  $reports.='<td>'.$timestamp.'</td>';
  $reports.='<td><form method="post">'.nonce();
  $reports.='<input type="hidden" name="report" value="'.$id.'" />';
  if($linksplit[0]=='comment')
  {
    $reports.='<button name="action" value="deletecomment">'._('Delete comment').'</button>';
  }else{
    $reports.='<button name="action" value="deletething">'._('Delete thing').'</button>';
    $reports.='<button name="action" value="ban">'._('Ban user').'</button>';
  }
  $reports.='<button name="action" value="deletereport">X</button>';
  $reports.='</form></td></tr>';
}
?>
<h1><?=_('Reports')?></h1>
<table>
  <tr>
    <th><?=_('Reported by')?></th>
    <th><?=_('Subject')?></th>
    <th><?=_('Reason')?></th>
    <th><?=_('Timestamp')?></th>
    <th></th>
  </tr>
  <?=$reports?>
</table>
<h1><?=_('Moderation log')?></h1>
<?=$modlog?>
