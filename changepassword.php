<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
if(isset($_COOKIE['PHPSESSID'])){session_start();}
if(!isset($_SESSION['id'])){header('Location: '.BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']));}
include_once('db.php');
include_once('nonce.php');
include_once('head.php');
$error='';
$msg='';
$oldpass='';
$pass='';
$pass2='';
if(isset($_POST['oldpass']) && isset($_POST['newpass']) && isset($_POST['newpass2']) && checknonce())
{
  $oldpass=$_POST['oldpass'];
  $pass=$_POST['newpass'];
  $pass2=$_POST['newpass2'];
  if(strlen($pass)<8){$error=_('Please use a safe password. Less than 8 characters is too short');}
  if($pass!=$pass2){$error=_('The new passwords don\'t match');}
  // Check oldpass
  $res=mysqli_query($db, 'select salt, password from users where id='.(int)$_SESSION['id']);
  $res=mysqli_fetch_assoc($res);
  if(!$res){die(_('DB error, changes have not been saved'));}
  $hash=explode(':', $res['password']);
  $oldhash=hash($hash[0], $oldpass.$res['salt']);
  if($oldhash!=$hash[1]){$error=_('Wrong password');}
  if($error=='')
  {
    $salt=bin2hex(random_bytes(32));
    $hash=HASH.':'.hash(HASH, $pass.$salt);
    mysqli_query($db, 'update users set salt="'.$salt.'", password="'.$hash.'" where id='.(int)$_SESSION['id']);
    $msg=_('Changed password');
    $oldpass='';
    $pass='';
    $pass2='';
  }
}
if($error!=''){$msg='<span class="error">'.$error.'</span>';}
?>
<center>
  <h1><?=_('Change password')?></h1>
  <?=$msg?>
</center>
<form method="post"><?=nonce();?>
  <div class="c2" style="text-align:right;"><?=_('Old password')?></div><div class="c2"><input type="password" name="oldpass" value="<?=$oldpass?>" /></div>
  <div class="c2" style="text-align:right;"><?=_('New password')?></div><div class="c2"><input type="password" name="newpass" value="<?=$pass?>" /></div>
  <div class="c2" style="text-align:right;"><?=_('New password again')?></div><div class="c2"><input type="password" name="newpass2" value="<?=$pass2?>" /></div>
  <center><button><?=_('Change')?></button></center>
</form>
