<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('rpc.php');
include_once('db.php');
$obj=rpc_verifypost($peer);
$user=mysqli_real_escape_string($db, $obj['user'].'@'.$peer);
$target=mysqli_real_escape_string($db, $obj['target']);
$reason=mysqli_real_escape_string($db, $obj['reason']);
$timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
if(mysqli_query($db, 'insert into reports(user, target, reason, timestamp) values("'.$user.'", "'.$target.'", "'.$reason.'", "'.$timestamp.'")'))
{
  print('{"status":"OK"}');
}else{
  print('{"error":"Database error"}');
}
?>
