<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('files.php');
// If you experience problem with slashes in searches you may need 'AllowEncodedSlashes NoDecode' in your Apache config (outside of <Directory>. More info at https://httpd.apache.org/docs/current/mod/core.html#allowencodedslashes )
$words=explode(' ', urldecode($path[3]));
$order=$path[4];
$count=(int)$path[5];
$skip=(int)$path[6];
if(!$count){$count=10;}
switch($order)
{
  case 'alpha': $order='name asc'; break;
  case 'old': $order='posted asc'; break;
  case 'new':
  default: $order='posted desc'; break;
}
// Find quotes and combine into one "word"
$qstart=false;
for($i=0; $i<count($words); ++$i)
{
  if(substr_count($words[$i],'"')==1 && $qstart!==false){$qstart=$i;} // Don't be picky about the qoute starting at the beginning or it'll break the modes and negative matches below
  elseif(substr($words[$i],-1)=='"') // End quote
  {
    $word=implode(' ', array_slice($words, $qstart, $i-$qstart+1));
    $word=str_replace('"','',$word); // Strip quotes
    array_splice($words, $qstart, $i-$qstart+1, $word);
    $qstart=false;
  }
}
$q='';
foreach($words as $word)
{
  // TODO: Avoid matching partial words?

  // Modes (e.g. name-only, description-only)
  $mode='';
  if(substr_count($word, ':'))
  {
    $mode=explode(':', $word)[0]; // Grab mode
    $word=substr($word, strlen($mode)+1); // Cut mode from word
  }
  // Negative matches
  $like='like';
  $or='or';
  $in='in';
  if(substr($word,0,1)=='-'){$like='not like'; $or='and'; $in='not in'; $word=substr($word,1);}
  // Escape
  $word=addcslashes(mysqli_real_escape_string($db, $word), '%_');
  if($q!=''){$q.=' and ';}
  switch($mode)
  {
    case 'name': $q.='(name '.$like.' "%'.$word.'%")'; break;
    case 'description': $q.='(description '.$like.' "%'.$word.'%")'; break;
    case 'tag':
      $ids=Array();
      $res=mysqli_query($db, 'select tagmaps.thing from tags, tagmaps where tagmaps.tag=tags.id and tags.name like "'.$word.'"');
      while($row=mysqli_fetch_row($res)){$ids[]=$row[0];}
      $q.='(id '.$in.' ('.implode(',', $ids).'))';
      break;
    default: $q.='(name '.$like.' "%'.$word.'%" '.$or.' description '.$like.' "%'.$word.'%")'; break;
  }
}
if($words==Array('')){$q='1';}
$obj=Array();
// Gather things
$res=mysqli_query($db, 'select id, thingid, name, description, posted, user from things where '.$q.' and latest and !removed order by '.$order.' limit '.$count.' offset '.$skip);
while($row=mysqli_fetch_assoc($res))
{
  $thing=Array('id'=>$row['thingid'],
               'name'=>$row['name'],
               'description'=>$row['description'],
               'date'=>$row['posted'],
               'tags'=>Array());
  $user=$row['user'];
  $revid=(int)$row['id'];
  // Grab preview from the chosen file, or the first file if none is chosen for preview
  $res2=mysqli_query($db, 'select name, hash from files where thing='.$revid.' order by preview desc limit 1');
  $row=mysqli_fetch_assoc($res2);
  $thing['preview']=getpreview($row['name'], $row['hash']);
  // Designer
  $res2=mysqli_query($db, 'select displayname, name from users where id='.$user);
  $row=mysqli_fetch_assoc($res2);
  $thing['by']=$row;
  // Tags
  $res2=mysqli_query($db, 'select tags.name from tags, tagmaps where tags.id=tagmaps.tag and tagmaps.thing='.$revid);
  while($row=mysqli_fetch_row($res2))
  {
    $thing['tags'][]=$row[0];
  }
  $obj[]=$thing;
}
print(json_encode($obj));
?>
