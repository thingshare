<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('rpc.php');
include_once('db.php');
$obj=rpc_verifypost($peer);
// Find recipient user ID
$to=mysqli_real_escape_string($db, $path[3]);
$res=mysqli_query($db, 'select id from users where name="'.$to.'"');
$res=mysqli_fetch_row($res);
if(!$res){header('HTTP/1.1 404 Not found'); die('{"error":"User not found"}');}
// Prepare values for DB
$user=(int)$res[0];
$recipient=mysqli_real_escape_string($db, $path[3].'@'.DOMAIN);
$sender=mysqli_real_escape_string($db, $obj['from'].'@'.$peer);
$subject=mysqli_real_escape_string($db, $obj['subject']);
$msg=mysqli_real_escape_string($db, $obj['message']);
$chain=mysqli_real_escape_string($db, $obj['chain']);
$timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
// Check that $user isn't blocking $sender
$res=mysqli_query($db, 'select user from userblocks where user='.$user.' and blocked="'.$sender.'" limit 1');
if(mysqli_fetch_row($res))
{
  die('{"error":"Communication blocked by user"}');
}
// Check that $user owns the chain and $sender is one of its participants (or that the chain doesn't exist yet)
$res=mysqli_query($db, 'select sender, recipient from messages where user='.$user.', chain="'.$chain.'" limit 1');
if($res=mysqli_fetch_row($res))
{
  if(!in_array($obj['from'].'@'.$peer, $res))
  {
    header('HTTP/1.1 400 Bad request'); die('{"error":"Invalid message chain"}');
  }
}
// Store message
$q='insert into messages(user, recipient, sender, sent, subject, message, msgread, latest, chain) ';
$q.='values('.$user.', "'.$recipient.'", "'.$sender.'", "'.$timestamp.'", "'.$subject.'", "'.$msg.'", false, true, "'.$chain.'")';
if(mysqli_query($db, $q))
{
  // Update 'latest' on messages which are now old
  mysqli_query($db, 'update messages set latest=false where chain="'.$chain.'" and user='.$user.' and id!='.(int)mysqli_insert_id($db));
  print('{"status":"OK"}');
}else{
  print('{"error":"Database error"}');
}
?>
