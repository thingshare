<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(isset($_POST['asyncrequest']))
{
  session_start();
}else{
  include_once('head.php');
}
include_once('db.php');
include_once('nonce.php');
include_once('rpc.php');

$thing=explode('@',$path[2]);
if(isset($_SESSION['id']))
{
  // Check if we're blocking the user (can't comment on things of people you're blocking)
  $by_esc=mysqli_real_escape_string($db, $thingobj['by']['name']);
  $res=mysqli_query($db, 'select user from userblocks where user='.(int)$_SESSION['id'].' and blocked="'.$by_esc.'" limit 1');
  $blocked=mysqli_fetch_row($res);
  $info='';
  $error='';
  if(checknonce())
  {
    if(isset($_POST['msg']))
    {
      if($blocked){$error=_('Cannot send messages to blocked users');}else{
        // Send it to thing's node
        $msg=Array('from'=>$_SESSION['name'],
                   'message'=>$_POST['msg'],
                   'replyto'=>$_POST['replyto']);
        $data=rpc_post($thing[1], 'comments/'.$thing[0], $msg);
        if(isset($data['error']))
        {
          $error=$data['error'];
          if(isset($_POST['asyncrequest'])){exit($error);}
        }else{
          rpc_cache($thing[1], 'comments/'.$thing[0], false); // Invalidate cache to show new comments
          if(isset($_POST['asyncrequest'])){exit('ok:'.$data['id']);}
          $info=_('Comment posted');
        }
      }
    }
    if(isset($_POST['tag_optin']))
    {
      $tag_name=mysqli_real_escape_string($db, $_POST['tag_optin']);
      $res=mysqli_query($db, 'select id from tags where optin!="" and name="'.$tag_name.'"');
      if($res=mysqli_fetch_row($res))
      {
        mysqli_query($db, 'insert into tag_optins(tag, user) values('.(int)$res[0].', '.(int)$_SESSION['id'].')');
      }
    }
  }
}
if(isset($_POST['asyncrequest'])){exit();}
$thingobj=rpc_get($thing[1], 'thing/'.$thing[0]);
if(isset($thingobj['error']))
{
  print('<div class="error"><h1>'.$thingobj['error'].'</h1></div>');
  include_once('foot.php');
  exit();
}
$name=htmlentities($thingobj['name']);
$by=htmlentities($thingobj['by']['name']);
$by='<a href="'.BASEURL.'/user/'.$by.'@'.$thing[1].'" title="'.$by.'@'.$thing[1].'">'.htmlentities($thingobj['by']['displayname']).'</a>';
$license=$thingobj['license']['name'];
if($license=='other')
{
  $license='Other, see description';
}else{
  $simple=(isset($thingobj['license']['simple'])?' title="'.htmlentities($thingobj['license']['simple']).'"':'');
  $license='<a href="https://'.$thing[1].'/license/'.urlencode($license).'"'.$simple.'>'.htmlentities($license).'</a>';
}
include_once('parsedown/Parsedown.php');
$md=new Parsedown();
$description=$md->text(htmlentities($thingobj['description']));
if(isset($_SESSION['name']))
{
  if($thing[1]==DOMAIN && $thingobj['by']['name']==$_SESSION['name'])
  {
    $description.='<a href="'.BASEURL.'/editthing/'.$thing[0].'">'._('Edit').'</a>';
  }else{
    $description.='<a href="'.BASEURL.'/report/thing/'.$thing[0].'@'.$thing[1].'">'._('Report').'</a>';
  }
}
$files='';
foreach($thingobj['files'] as $file)
{
  $type=(isset($file['type'])?$file['type']:'unknown');
  $files.='<div class="thing">';
  if(isset($file['preview3d'])){$files.='<div class="boxtop" style="right:0px;"><a href="#" onclick="return threedview(this,'.PREVIEW_SIZE[0].','.PREVIEW_SIZE[1].');" data-file="https://'.$thing[1].$file['preview3d'].'">3D view</a></div>';}
  $files.='<a href="'.abslink($thing[1], $file['path']).'" download="'.htmlentities($file['name']).'" type="'.$type.'"><div class="boxbottom">'.htmlentities($file['name']).'</div><img src="'.abslink($thing[1], $file['preview']).'" /></a></div>';
}
$tags='';
foreach($thingobj['tags'] as $tag)
{
  if(!isset($_GET['show_'.strtolower($tag).'_content']) || $_GET['show_'.strtolower($tag).'_content']!='true')
  {
    // Check if tag requires optin
    $tagname=mysqli_real_escape_string($db, $tag);
    $res=mysqli_query($db, 'select id, optin from tags where name="'.$tagname.'"');
    $optin=mysqli_fetch_assoc($res);
    if($optin['optin']!='' && isset($_SESSION['id'])) // Check if user is already opted in
    {
      $res=mysqli_query($db, 'select tag from tag_optins where tag='.(int)$optin['id'].' and user='.(int)$_SESSION['id']);
      if(mysqli_fetch_row($res)){$optin['optin']='';} // Just act like the optin doesn't exist
    }
    if($optin['optin']!='')
    {
      $showurl=$_SERVER['REQUEST_URI'];
      $showurl.=(substr_count($showurl, '?')?'&':'?').'show_'.strtolower($tag).'_content=true';
      print('<center>'.$optin['optin'].'<br />');
      print('<a href="'.$showurl.'"><button>'.sprintf(_('Show %s content'), htmlentities($tag)).'</button></a>');
      if(isset($_SESSION['id']))
      {
        print('<form method="post" style="display:inline;">'.nonce());
        print('<button name="tag_optin" value="'.htmlentities($tag).'">'.sprintf(_('Always show %s content'), htmlentities($tag)).'</button>');
        print('</form>');
      }
      print('</center>');
      include_once('footer.php');
      exit();
    }
  }
  $tag=htmlentities($tag);
  $tags.=' <a href="'.BASEURL.'/tag/'.$tag.'">'.$tag.'</a>';
}
$comments=rpc_get($thing[1], 'comments/'.$thing[0]);
include_once('parsedown/Parsedown.php');
$md=new Parsedown();
function commenthasreply($comment) // Search for undeleted replies
{
  foreach($comment['replies'] as $reply)
  {
    if(substr_count($reply['sender'], '@')>0){return true;}
    if(commenthaschild($reply)){return true;}
  }
  return false;
}
function printcomments($comments, $level=0)
{
  global $md;
  global $thing;
  foreach($comments as $comment)
  {
    $sender=htmlentities($comment['sender']);
    $senderhref='href="'.BASEURL.'/user/'.$sender.'"';
    if(substr_count($sender, '@')>0)
    {
      $displayname=htmlentities(getdisplayname($comment['sender']));
    }else{
      if(!commenthasreply($comment)){continue;} // Don't print fully deleted chains
      $senderhref='';
      $displayname=$sender;
    }
    $msg=$md->text(htmlentities($comment['message']));
    $time=htmlentities($comment['sent']);
    $timeago=timeago($comment['sent']);
    print('<div class="message" style="margin-left:'.($level*15).'px;" data-id="'.$comment['id'].'"><div class="message_sender"><a '.$senderhref.' title="'.$sender.'" name="comment'.$comment['id'].'">'.$displayname.'</a> <span class="time" title="'.$time.' UTC">'.$timeago.'</span></div><span>'.$msg.'</span><p>');
    if(substr_count($sender, '@')>0 && isset($_SESSION['id']))
    {
      print('<a href="#" onclick="replyto(this.parentElement.parentElement); return false;">'._('Reply').'</a>');
      print(' - <a href="'.BASEURL.'/report/comment/'.$thing[0].'/'.$comment['id'].'@'.$thing[1].'">Report</a>');
    }
    print('</p></div>');
    printcomments($comment['replies'], $level+1);
  }
}
if($error!=''){$info='<span class="error">'.$error.'</span>';}
?>
<script>
<!--
function replyto(comment)
{
  document.getElementById('replyto').value=comment.dataset.id;
  document.getElementById('replyindicator').innerHTML='Replying to:<br />'+comment.children[1].innerHTML;
  document.getElementById('commentmsg').focus();
}
// -->
</script>
<h1><?=$name?> <small class="subheader">by <?=$by?></small></h1>
<small><?=sprintf(_('Published %s under the license %s'), '<span class="time" title="'.htmlentities($thingobj['date']).' UTC">'.timeago($thingobj['date']).'</span>', $license)?></small><br />
<?=$description?><br />
<?=_('Tags:')?> <?=$tags?><br />
<?=$files?>
<h2><?=_('Comments')?></h2>
<?=$info?>
<?=printcomments($comments)?>
<?php if(isset($_SESSION['id'])){ ?>
<script src="<?=BASEURL?>/mdjs/mdjs.js"></script>
<form method="post" action="<?=BASEURL?>/thing/<?=$path[2]?>" id="commentform">
  <p>
    <?=nonce()?>
    <?php if(!$blocked){ ?>
    <input type="hidden" id="replyto" name="replyto" value="" />
    <div id="replyindicator"></div>
    <textarea rows="4" style="width:100%;" id="commentmsg" name="msg" onchange="document.getElementById('mdpreview').innerHTML='Markdown preview:<br />'+Mdjs.md2html(this.value.replace(/&/g,'&amp;amp;').replace(/</g,'&amp;lt;'));" onkeyup="this.onchange();"></textarea>
    <div id="mdpreview"></div>
    <button><?=_('Send')?></button>
    <?php } ?>
  </p>
</form>
<?php } ?>
