<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('head.php');
include_once('rpc.php');
// Gather the 10 most recent local things
$things='';
$res=rpc_get(DOMAIN, 'search//newest/10/0');
foreach($res as $thing)
{
  $by=htmlentities($thing['by']['name']);
  $by='<a href="'.BASEURL.'/user/'.$by.'@'.DOMAIN.'" title="'.$by.'@'.DOMAIN.'">'.htmlentities($thing['by']['displayname']).'</a>';
  $things.='<div class="thing"><div class="boxtop">By '.$by.'</div><a href="'.BASEURL.'/thing/'.$thing['id'].'@'.DOMAIN.'/'.urlencode(str_replace(' ','_',$thing['name'])).'"><div class="boxbottom">'.htmlentities($thing['name']).'</div><img src="https://'.DOMAIN.$thing['preview'].'" /></a></div>';
}
// Get statistics
function getcount($table)
{
  global $db;
  $res=mysqli_query($db, 'select count(*) from '.$table);
  $res=mysqli_fetch_row($res);
  return $res[0];
}
$numusers=getcount('users where status='.ACCOUNT_ACTIVE);
$numthings=getcount('things where latest and !removed');
$numpeers=getcount('peers where !blacklist');
?>
<h1><?=sprintf(_('Latest designs on %s'), DOMAIN)?></h1>
<?=$things?>

<h1><?=_('Statistics')?></h1>
<?=sprintf(_('%d users, %d things on %s. Federating with %d peers'), $numusers, $numthings, DOMAIN, $numpeers)?>
