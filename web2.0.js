/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2021  Alicia <alicia@ion.nu>

    See /COPYING for license text (AGPLv3+)
*/
function postrequest(url, body, callback)
{
  if(fetch)
  {
    var options={'method':'POST', 'body':body, 'credentials':'same-origin', 'headers':{'Content-type':'application/x-www-form-urlencoded'}};
    fetch(url, options).then(x=>x.text()).then(callback);
  }else{
    var req=new XMLHttpRequest();
    req.open('POST',url,true);
    req.onreadystatechange=function()
    {
      if(this.readyState==4){callback(this.responseText);}
    };
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.send(body);
  }
}

window.addEventListener('load', function()
{
  var commentform=document.getElementById('commentform');
  if(commentform)
  {
    commentform.addEventListener('submit', function(evt)
    {
      var button=commentform.elements[commentform.elements.length-1];
      button.textContent='Sending...'; // TODO: Localization?
      button.disabled=true;
      // Submit form
      var nonce=encodeURIComponent(commentform.nonce.value);
      var replyid=encodeURIComponent(commentform.replyto.value);
      var msg=encodeURIComponent(commentform.msg.value);
      postrequest('', 'nonce='+nonce+'&replyto='+replyid+'&msg='+msg+'&asyncrequest', function(response)
      {
        button.textContent='Send'; // TODO: Localization?
        button.disabled=false;
        // Handle response
        if(response.substring(0,3)=='ok:')
        {
          // Insert message
          var level=0;
          var prev=false;
          if(replyid>0) // Figure out the indentation level
          {
            prev=document.getElementsByName('comment'+replyid);
            if(prev[0]){level=parseInt(prev[0].parentElement.parentElement.style.marginLeft)+15;}
          }
          var div=document.createElement('div');
          div.className='message';
          div.style.marginLeft=level+'px';
          div.dataset.id=response.substring(3);
          var div2=document.createElement('div');
          div2.className='message_sender';
          // User link
          var profilelink=document.getElementById('profilelink');
          var link=document.createElement('a');
          link.name='comment'+response.substring(3);
          link.href=profilelink.href;
          link.textContent=profilelink.textContent;
          div2.appendChild(link);
          div2.appendChild(document.createTextNode(' '));
          // Timestamp
          var span=document.createElement('span');
          span.className='time';
          span.textContent='just now';
          div2.appendChild(span);
          div.appendChild(div2);
          // Message
          span=document.createElement('span');
          span.innerHTML=Mdjs.md2html(commentform.msg.value.replace(/&/g,'&amp;amp;').replace(/</g,'&amp;lt;'));
          div.appendChild(span);
          span=document.createElement('p');
          link=document.createElement('a');
          link.href='#';
          link.onclick=function(){replyto(this.parentElement.parentElement); return false;};
          link.textContent='Reply';
          span.appendChild(link);
          div.appendChild(span);
          var content=document.getElementById('content');
          if(prev && prev[0] && prev[0].parentElement.parentElement.nextElementSibling)
          {
            content.insertBefore(div, prev[0].parentElement.parentElement.nextElementSibling);
          }else{
            content.insertBefore(div, commentform);
          }
          // Clear form
          commentform.replyto.value='';
          commentform.msg.value='';
          document.getElementById('mdpreview').innerHTML='';
          document.getElementById('replyto').value='';
          document.getElementById('replyindicator').innerHTML='';
        }else{ // TODO: display error better
          alert('Error: '+response);
        }
      });
      evt.preventDefault();
    });
  }
});
