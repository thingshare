<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
function getfilepath($hash, $serverside=false)
{
  $hash=explode(':', $hash);
  $path='files/'.$hash[0];
  $hash=$hash[1];
  if(!$serverside){$path=BASEURL.'/'.$path;}
  for($i=0; $i<HASHTREE_LEVELS; ++$i)
  {
    $path.='/'.substr($hash, 0, HASHTREE_SECTION);
    $hash=substr($hash, HASHTREE_SECTION);
  }
  $path.='/'.$hash;
  return $path;
}
function getpreview($name, $hash)
{
  $file=getfilepath($hash, true);
  if(file_exists($file.'.png')){return BASEURL.'/'.$file.'.png';}
  // No actual preview found, get a filetype icon
  $dot=strrpos($name, '.');
  $filetype=strtolower(substr($name, $dot+1));
  if(file_exists('icons/'.$filetype.'.svg')){return BASEURL.'/icons/'.$filetype.'.svg';}
  if(file_exists('icons/'.$filetype.'.png')){return BASEURL.'/icons/'.$filetype.'.png';}
  return BASEURL.'/icons/default.svg';
}
function get3dpreview($name, $hash)
{
  $file=getfilepath($hash, true);
  if(file_exists($file.'.x3d')){return BASEURL.'/'.$file.'.x3d';}
  return false;
}
?>
