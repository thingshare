<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_PRIVILEGES)){die(_('Insufficient privileges'));}

if(checknonce()) // Save changes
{
  $priv=0;
  foreach($_POST['privileges'] as $p){$priv+=$p;}
  $q='';
  if(isset($_POST['addprivileges']) && $_POST['addprivileges']!='')
  {
    $user=mysqli_real_escape_string($db, $_POST['addprivileges']);
    $q.='name="'.$user.'"';
  }
  elseif(isset($_POST['editprivileges'])){$q='id='.(int)$_POST['editprivileges'];}
  if($q!='')
  {
    mysqli_query($db, 'update users set privileges='.(int)$priv.' where '.$q);
  }
}

// Load current
$users='';
$res=mysqli_query($db, 'select id, name, displayname, privileges from users where privileges>0 order by privileges desc');
while($row=mysqli_fetch_assoc($res))
{
  $name=htmlentities($row['name']);
  $displayname=htmlentities($row['displayname']);
  $users.='<tr>';
  $users.='  <td><a href="'.BASEURL.'/user/'.$name.'@'.DOMAIN.'" title="'.$name.'@'.DOMAIN.'">'.$displayname.'</a></td>';
  $users.='  <td><form method="post">'.nonce();
  foreach(PRIV_NAMES as $n=>$priv)
  {
    $checked=(($row['privileges']&(2**$n))?' checked':'');
    $users.='<label><input type="checkbox" name="privileges[]" value="'.(2**$n).'"'.$checked.' />';
    $users.=$priv.'</label><br />';
  }
  $users.='  <button name="editprivileges" value="'.$row['id'].'">'._('Save').'</button></form></td>';
  $users.='</tr>';
}
// Gather privileges for new admins
$privileges='';
foreach(PRIV_NAMES as $n=>$priv)
{
  $privileges.='<label><input type="checkbox" name="privileges[]" value="'.(2**$n).'" />';
  $privileges.=$priv.'</label><br />';
}
?>
<h2><?=_('Privileges')?></h2>
<table>
  <tr>
    <th><?=_('User')?></th>
    <th><?=_('Privileges')?></th>
  </tr>
  <?=$users?>
</table>
<form method="post"><?=nonce()?>
  <input type="text" name="addprivileges" placeholder="<?=_('Username')?>" /><br />
  <?=$privileges?>
  <button><?=('Save')?></button>
</form>
