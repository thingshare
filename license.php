<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// Note: this file is not a license text, it's merely a script that handles license texts for 'things'
include_once('db.php');
$name=mysqli_real_escape_string($db, urldecode($path[2]));
$res=mysqli_query($db, 'select full from licenses where name="'.$name.'"');
if(!($res=mysqli_fetch_row($res)))
{
  header('HTTP/1.1 404 Not found');
  include_once('head.php');
  print('<div class="error"><h1>'._('License not found').'</h1></div>');
}else{
  $full=$res[0];
  if(substr_count($full, '://')>0 && substr_count($full, "\n")==0)
  {
    // If 'full license text' is specified as a link, forward visitor to that link
    header('Location: '.$full);
  }else{
    print($full);
  }
  exit();
}
?>
