<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('rpc.php');
include_once('db.php');
$thing=(int)$path[3];
if(isset($_POST['data'])) // Posting a comment
{
  $obj=rpc_verifypost($peer);
  // Find thing's user (and check that thing exists)
  $res=mysqli_query($db, 'select user from things where thingid='.$thing.' limit 1');
  $res=mysqli_fetch_row($res);
  if(!$res){header('HTTP/1.1 404 Not found'); die('{"error":"Thing not found"}');}
  // Prepare values for DB
  $user=(int)$res[0];
  $sender=mysqli_real_escape_string($db, $obj['from'].'@'.$peer);
  $msg=mysqli_real_escape_string($db, $obj['message']);
  $replyto=(int)$obj['replyto'];
  $timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
  // Check that $user isn't blocking $sender
  $res=mysqli_query($db, 'select user from userblocks where user='.$user.' and blocked="'.$sender.'" limit 1');
  if(mysqli_fetch_row($res))
  {
    die('{"error":"Communication blocked by user"}');
  }
  if($replyto) // Check that the replied-to comment exists (if it's a reply)
  {
    $res=mysqli_query($db, 'select id from comments where id='.$replyto);
    if(!mysqli_fetch_row($res))
    {
      header('HTTP/1.1 404 Not found');
      die('{"error":"Replied-to comment not found"}');
    }
  }
  // Store message
  $q='insert into comments(thing, sender, replyto, message, sent, removed) ';
  $q.='values('.$thing.', "'.$sender.'", '.$replyto.', "'.$msg.'", "'.$timestamp.'", false)';
  if(mysqli_query($db, $q))
  {
    print('{"status":"OK","id":'.mysqli_insert_id($db).'}');
    // Notify the parent commenter or thing's owner
    $link='/thing/'.$thing.'@'.DOMAIN.'#comment'.mysqli_insert_id($db);
    $name=getdisplayname($obj['from'].'@'.$peer);
    $res=mysqli_query($db, 'select name, user from things where thingid='.$thing.' order by posted desc limit 1');
    $thingrow=mysqli_fetch_assoc($res);
    $thingname=$thingrow['name'];
    if($replyto)
    {
      $res=mysqli_query($db, 'select sender from comments where id='.$replyto);
      $row=mysqli_fetch_row($res);
      $touser=explode('@', $row[0]);
      if(count($touser)==2)
      {
        $msg=Array('message'=>$name.' replied to your comment on '.$thingname,
                   'link'=>$link);
        rpc_post($touser[1], 'notification/'.$touser[0], $msg);
      }
    }else{
      $msg=Array('message'=>$name.' commented on '.$thingname,
                 'link'=>$link);
      rpc_post(DOMAIN, 'notification/'.db_getuser($thingrow['user']), $msg);
    }
  }else{
    print('{"error":"Database error"}');
  }
}else{ // Get comments
  $comments=Array();
  $replies=Array();
  function resolvereplies($id)
  {
    global $replies;
    $r=Array();
    foreach($replies as $i)
    {
      if($i['replyto']==$id)
      {
        $i['replies']=resolvereplies($i['id']);
        $r[]=$i;
      }
    }
    return $r;
  }
  $res=mysqli_query($db, 'select id, sender, replyto, message, sent, removed from comments where thing='.$thing.' order by sent asc');
  while($row=mysqli_fetch_assoc($res))
  {
    if($row['removed'])
    {
      $row['sender']='removed';
      $row['message']='removed';
    }
    if($row['replyto']==0)
    {
      $comments[]=$row;
    }else{
      $replies[]=$row;
    }
  }
  foreach($comments as $i=>$c){$comments[$i]['replies']=resolvereplies($comments[$i]['id']);}
  print(json_encode($comments));
}
?>
