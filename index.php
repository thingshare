<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// <h1>If you're seeing this, PHP isn't set up correctly</h1><!--
// Upgrade HTTP requests to HTTPS if the client supports/requests it
if(isset($_SERVER['HTTP_UPGRADE_INSECURE_REQUESTS']) &&
   in_array(strtolower($_SERVER['HTTP_UPGRADE_INSECURE_REQUESTS']), Array('1','true','yes','on')) &&
   (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS']=='off'))
{
  header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], true, 301);
  exit();
}
if(!file_exists('config.php')){include('setup.php'); exit();}
include_once('config.php');
$path=$_SERVER['REQUEST_URI'];
if(strpos($path, '?')){$path=substr($path, 0, strpos($path, '?'));}
if(substr($path, 0, strlen(BASEURL))==BASEURL){$path=substr($path, strlen(BASEURL));}
$path=explode('/', $path);
switch($path[1])
{
  case 'login': include('login.php'); break;
  case 'logout': include('logout.php'); break;
  case 'register': include('register.php'); break;
  case 'user': include('user.php'); break;
  case 'thing': include('thing.php'); break;
  case 'browse':
  case 'tag':
  case 'search': include('search.php'); break;
  case 'license': include('license.php'); break;
  case 'editprofile': include('editprofile.php'); break;
  case 'editthing': include('editthing.php'); break;
  case 'changepassword': include('changepassword.php'); break;
  case 'messages': include('messages.php'); break;
  case 'verifyemail': include('verifyemail.php'); break;
  case 'report': include('report.php'); break;
  case 'admin': include('admin.php'); break;
  case 'rpc':
    header('Content-type: application/json');
    include_once('db.php');
    if(isset($_SERVER['HTTP_X_THINGSHARE_NODE']) && $_SERVER['HTTP_X_THINGSHARE_NODE']!=DOMAIN && getoption('autofollow'))
    {
      $peer=mysqli_real_escape_string($db, $_SERVER['HTTP_X_THINGSHARE_NODE']);
      $res=mysqli_query($db, 'select domain from peers where domain="'.$peer.'"');
      if(!mysqli_fetch_row($res))
      {
        mysqli_query($db, 'insert into peers(domain, blacklist) values("'.$peer.'", 0)');
      }
    }
    switch($path[2])
    {
      case 'user': include('rpc_user.php'); exit();
      case 'thing': include('rpc_thing.php'); exit();
      case 'search': include('rpc_search.php'); exit();
      case 'rpckey': print(json_encode(Array('public'=>file_get_contents('rpckey.pem')), true)); exit();
      case 'report': include('rpc_report.php'); exit();
      case 'messages': include('rpc_messages.php'); exit();
      case 'peers': include('rpc_peers.php'); exit();
      case 'comments': include('rpc_comments.php'); exit();
      case 'notification': include('rpc_notification.php'); exit();
    }
    header('HTTP/1.1 404 Not found');
    die('{"httpresponse":404,"error":"Not found"}');
    break;
  case '': include('start.php'); break;
  default:
    include_once('head.php');
    header('HTTP/1.1 404 Not found');
    print('<div class="error"><h1>Not found</h1></div>');
    break;
}
include_once('foot.php');
?>
