<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
$error='';
if(!isset($_GET['u']) || !isset($_GET['c']))
{
  $error=_('No verification code specified');
}else{
  include_once('db.php');
  $user=mysqli_real_escape_string($db, $_GET['u']);
  $res=mysqli_query($db, 'select salt, email, status, id from users where name="'.$user.'"');
  $res=mysqli_fetch_assoc($res);
  if($res['status']!=ACCOUNT_EMAILUNVERIFIED)
  {
    $error=_('This e-mail address is already verified');
  }else{
    $salt=$res['salt'];
    $email=$res['email'];
    $error=_('Invalid verification code');
    foreach(Array(-1,0,1) as $day) // Allow some drift for timezone borders
    {
      $verifycode=hash(HASH, $email.$salt.date('j', time()+(24*3600)*$day));
      if($_GET['c']==$verifycode)
      {
        $error='';
        mysqli_query($db, 'update users set status='.(int)ACCOUNT_ACTIVE.' where id='.(int)$res['id']);
      }
    }
  }
}
include_once('head.php');
if($error!='')
{
  print('<center><div class="error">'.$error.'</div></center>');
}else{
  print('<center><h1>'._('Verification complete').'</h1>You may now <a href="'.BASEURL.'/login">log in</a></center>');
}
?>
