<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('rpc.php');
include_once('db.php');
$obj=rpc_verifypost($peer);
// Find recipient user ID
$to=mysqli_real_escape_string($db, $path[3]);
$res=mysqli_query($db, 'select id from users where name="'.$to.'"');
$res=mysqli_fetch_row($res);
if(!$res){header('HTTP/1.1 404 Not found'); die('{"error":"User not found"}');}
// TODO: Check that the link refers to something on the sender node?
// Prepare values for DB
$user=(int)$res[0];
$msg=mysqli_real_escape_string($db, $obj['message']);
$link=mysqli_real_escape_string($db, $obj['link']);
$timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
// Store notification
$q='insert into notifications(user, message, link, sent, seen) ';
$q.='values('.$user.', "'.$msg.'", "'.$link.'", "'.$timestamp.'", false)';
if(mysqli_query($db, $q))
{
  print('{"status":"OK"}');
}else{
  print('{"error":"Database error"}');
}
?>
