<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
define('PRIV_PRIVILEGES', 1); // Privilege of managing privileges
define('PRIV_MODERATE', 2);   // Content moderation, deleting 'things' and comments, adding/removing tags on things, banning users. Reading reports
define('PRIV_TAGS', 4);       // Managing tags (TODO)
define('PRIV_FEDERATION', 8); // Control who to federate with (blacklists, auto)
define('PRIV_FILETYPES', 16); // Manage filetypes
define('PRIV_LICENSES', 32);  // Manage the license list

define('PRIV_ALL', 63);       // Convenience macro for all privileges
define('PRIV_NAMES', Array('Privilege of managing privileges',
                           'Content moderation',
                           'Tag management',
                           'Federation',
                           'Manage filetypes',
                           'Manage the license list'));

define('ACCOUNT_ACTIVE', 0);
define('ACCOUNT_BANNED', 1);
define('ACCOUNT_EMAILUNVERIFIED', 2);

$db=mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
date_default_timezone_set('UTC'); // Anything involving the database should use UTC time

function getoption($name, $default=false)
{
  global $db;
  $name=mysqli_real_escape_string($db, $name);
  $res=mysqli_query($db, 'select value from options where name="'.$name.'"');
  if($res=mysqli_fetch_row($res))
  {
    return $res[0];
  }
  return $default;
}

function setoption($name, $value)
{
  global $db;
  if($value===false){$value='0';}
  elseif($value===true){$value='1';}
  $name=mysqli_real_escape_string($db, $name);
  $value=mysqli_real_escape_string($db, $value);
  mysqli_query($db, 'update options set value="'.$value.'" where name="'.$name.'"');
  if(substr_count(mysqli_info($db), 'Rows matched: 0')) // None to update, insert new
  {
    mysqli_query($db, 'insert into options(name, value) values("'.$name.'", "'.$value.'")');
  }
}

function db_create_tables()
{
  global $db;
  mysqli_query($db, 'create table users(
    id integer primary key auto_increment,
    name varchar(64) unique,
    salt text,
    password text,
    email text,
    displayname text,
    profile text,
    privileges integer,
    status integer);');
  mysqli_query($db, 'create table things(
    id integer primary key auto_increment,
    thingid integer,
    user integer,
    name text,
    description text,
    posted datetime,
    latest boolean,
    removed boolean,
    license text);');
  mysqli_query($db, 'create table files(
    id integer primary key auto_increment,
    thing integer,
    name text,
    hash text,
    preview boolean);');
  mysqli_query($db, 'create table options(name text, value text);');
  mysqli_query($db, 'create table peers(domain text, blacklist boolean);');
  mysqli_query($db, 'create table cooldown(domain text, end datetime);');
  mysqli_query($db, 'create table rpccache(hash varchar(500) primary key, timestamp datetime, cache text);');
  mysqli_query($db, 'create table reports(
    id integer primary key auto_increment,
    user text,
    target text,
    reason text,
    timestamp datetime);');
  mysqli_query($db, 'create table moderationlog(user integer, timestamp datetime, action text, target text);');
  mysqli_query($db, 'create table filetypes(extension varchar(500) primary key, mimetype text);');
  mysqli_query($db, 'create table licenses(name varchar(500) primary key, simple text, full text, removed boolean, defaultlicense boolean);'); // Long 'defaultlicense' because 'default' is a keyword
  mysqli_query($db, 'create table messages(id integer primary key auto_increment,
    user integer,
    recipient text,
    sender text,
    sent datetime,
    chain varchar(500),
    subject text,
    message text,
    msgread boolean,
    latest boolean);');
  mysqli_query($db, 'create table userblocks(user integer, blocked text);');
  mysqli_query($db, 'create table loginfails(ip varchar(256), timestamp datetime);');
  mysqli_query($db, 'create table tags(id integer primary key auto_increment, name text, blacklist boolean, optin text);');
  mysqli_query($db, 'create table tagmaps(tag integer, thing integer);');
  mysqli_query($db, 'create table tag_optins(tag integer, user integer);');
  mysqli_query($db, 'create table comments(id integer primary key auto_increment,
    thing integer,
    sender text,
    replyto integer,
    message text,
    sent datetime,
    removed boolean);');
  mysqli_query($db, 'create table filtertags(tag text);');
  mysqli_query($db, 'create table notifications(id integer primary key auto_increment,
    user integer,
    message text,
    link text,
    sent datetime,
    seen boolean);');
}

function db_getuser($id)
{
  // Small cache in case the same user's name needs to be resolved multiple times on the same page
  static $cache=Array();
  if(isset($cache[$id])){return $cache[$id];}
  // Fetch from DB and cache for later
  global $db;
  $res=mysqli_query($db, 'select name from users where id='.(int)$id);
  $res=mysqli_fetch_row($res);
  $cache[$id]=$res[0];
  return $res[0];
}

function db_getmimetype($ext)
{
  global $db;
  $dot=strrpos($ext, '.');
  if($dot!==false){$ext=substr($ext, $dot+1);}
  $ext=mysqli_real_escape_string($db, strtolower($ext));
  // Small cache in case the same extension's MIME type needs to be resolved multiple times on the same page
  static $cache=Array();
  if(isset($cache[$ext])){return $cache[$ext];}
  $res=mysqli_query($db, 'select mimetype from filetypes where extension="'.$ext.'"');
  if($res=mysqli_fetch_row($res))
  {
    $cache[$ext]=$res[0];
    return $res[0];
  }
  return false;
}
?>
