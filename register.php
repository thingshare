<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('head.php');
include_once('db.php');
$user='';
$email='';
$pass='';
$pass2='';
$error='';
$form=true;
if(isset($_POST['user']) && isset($_POST['email']) && isset($_POST['pass']) && isset($_POST['pass2']))
{
  $user=$_POST['user'];
  $email=$_POST['email'];
  $pass=$_POST['pass'];
  $pass2=$_POST['pass2'];
  // Error checks
  if(!isset($_POST['tos'])){$error=_('Agreeing to the terms of service is mandatory for membership');}
  if(substr_count($email, '@')!=1){$error=sprintf(_('%s does not appear to be a valid e-mail address'), $email);}
  if($pass!=$pass2){$error=_('The passwords don\'t match');}
  if(strlen($pass)<8){$error=_('Please use a safe password. Less than 8 characters is too short');}
  if($user==''){$error=_('Please pick a username');}
  foreach(Array('@',' ','/') as $char) // Check for disallowed characters
  {
    if(substr_count($user, $char)>0){$error=sprintf(_("Usernames may not contain '%s'"), $char);}
  }
  $user_esc=mysqli_real_escape_string($db, $user);
  $email_esc=mysqli_real_escape_string($db, $email);
  $res=mysqli_query($db, 'select name from users where name="'.$user_esc.'"');
  if(mysqli_fetch_row($res)){$error=_('A user by that name already exists on this node. Please pick a different username');}
  $res=mysqli_query($db, 'select name from users where email="'.$email_esc.'"');
  if(mysqli_fetch_row($res)){$error=_('A user with that e-mail address already exists on this node');}
  if($error=='')
  {
    $salt=bin2hex(random_bytes(32));
    $hash=HASH.':'.hash(HASH, $pass.$salt);
    $usercount=mysqli_query($db, 'select count(*) from users');
    $usercount=mysqli_fetch_row($usercount)[0];
    $privileges=($usercount==0?PRIV_ALL:0); // First one in is an administrator
    mysqli_query($db, 'insert into users(name, salt, password, email, displayname, profile, privileges, status) values("'.$user_esc.'", "'.$salt.'", "'.$hash.'", "'.$email_esc.'", "'.$user_esc.'", "", '.$privileges.', '.ACCOUNT_EMAILUNVERIFIED.')');
    // Email verification
    $verifycode=hash(HASH, $email.$salt.date('j'));
    // TODO: HTML email? Alternative email methods? PHPMailer seems popular but I'm not sure what the need or appeal is
    mail($email, _('Thingshare e-mail verification'), _('Please follow this link to verify your e-mail address and activate your Thingshare account:')."\nhttps://".$_SERVER['HTTP_HOST'].BASEURL.'/verifyemail?u='.urlencode($user).'&c='.$verifycode, Array('From'=>NODENAME.' <'.EMAIL.'>', 'Date'=>date('r')));
    print('<h1>'._('Welcome!').'</h1>');
    print(_('You will shortly receive a verification e-mail, please follow the link to activate your account'));
    $form=false;
  }
}
if($error!=''){$error='<div class="error">'.$error.'</div>';}
if($form)
{
?>
<br />
<form method="post">
  <center>
    <?=$error?>
    <input type="text" name="user" placeholder="Username" value="<?=$user?>" /><br />
    <input type="text" name="email" placeholder="E-mail address" value="<?=$email?>" /><br />
    <input type="password" name="pass" placeholder="Password" value="<?=$pass?>" /><br />
    <input type="password" name="pass2" placeholder="Password again" value="<?=$pass2?>" /><br />
    <label><input type="checkbox" name="tos"<?=(isset($_POST['tos'])?' checked':'')?> /> I agree to the <a href="termsofservice.html" target="_blank">terms of service</a></label><br />
    <button><?=_('Create account')?></button>
  </center>
<form>
<?php } ?>
