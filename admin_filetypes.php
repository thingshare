<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_FILETYPES)){die(_('Insufficient privileges'));}
if(checknonce()) // Save changes
{
  if(isset($_POST['action']) && $_POST['action']=='newfiletype')
  {
    $extension=mysqli_real_escape_string($db, $_POST['newextension']);
    $mimetype=mysqli_real_escape_string($db, $_POST['newmimetype']);
    mysqli_query($db, 'insert into filetypes(extension, mimetype) values("'.$extension.'", "'.$mimetype.'")');
  }
  if(isset($_POST['removefiletype']))
  {
    $extension=mysqli_real_escape_string($db, $_POST['removefiletype']);
    mysqli_query($db, 'delete from filetypes where extension="'.$extension.'"');
  }
}

// Load current
$filetypes='';
$res=mysqli_query($db, 'select extension, mimetype from filetypes order by extension asc');
while($row=mysqli_fetch_assoc($res))
{
  $extension=htmlentities($row['extension']);
  $mimetype=htmlentities($row['mimetype']);
  $filetypes.='<tr>';
  $filetypes.='  <td>'.$extension.'</td>';
  $filetypes.='  <td>'.$mimetype.'</td>';
  $filetypes.='  <td><button name="removefiletype" value="'.$extension.'">X</button></td>';
  $filetypes.='</tr>';
}
?>
<h1><?=_('Filetypes')?></h1>
<p><?=('This list serves a dual purpose of providing MIME types and controlling which files can be uploaded')?></p>
<form method="post"><?=nonce()?>
  <table>
    <tr>
      <th><?=_('Filename extension')?></th>
      <th><?=_('MIME type')?></th>
      <th></th>
    </tr>
    <?=$filetypes?>
  </table>
</form>
<form method="post"><?=nonce()?>
  <?=_('Filename extension')?>: <input type="text" name="newextension" /><br />
  <?=_('MIME type')?>: <input type="text" name="newmimetype" /><br />
  <button name="action" value="newfiletype"><?=_('Add')?></button>
</form>
