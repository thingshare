<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2022  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('head.php');
include_once('db.php');
include_once('rpc.php');
// Get list of federated nodes
$peers=Array(DOMAIN);
$res=mysqli_query($db, 'select domain from peers where !blacklist');
while($row=mysqli_fetch_row($res)){$peers[]=$row[0];}
// Get list of tags to filter out
$filtertags='';
$res=mysqli_query($db, 'select tag from filtertags');
while($row=mysqli_fetch_row($res)){$filtertags.=' tag:-'.$row[0];}
// Get list of tags to hide content for (e.g. 'nsfw')
$hiddentags=Array();
$res=mysqli_query($db, 'select name, id from tags where optin!=""');
while($row=mysqli_fetch_assoc($res))
{
  if(isset($_SESSION['id'])) // Check if user opted in
  {
    $res2=mysqli_query($db, 'select tag from tag_optins where tag='.(int)$row['id'].' and user='.(int)$_SESSION['id']);
    if(mysqli_fetch_row($res2)){continue;}
  }
  $hiddentags[]=$row['name'];
}
// Pagination
$perpage=(isset($_GET['perpage'])?(int)$_GET['perpage']:20);
$page=(int)(isset($_GET['page'])?$_GET['page']:0);
$perpeer=round($perpage/count($peers));
$sortby=(isset($_GET['sort'])?$_GET['sort']:'new');
if($path[1]=='browse'){$sortby=$path[2];} // Handle 'Browse' links
else if($path[1]=='tag'){$_GET['q']='tag:'.$path[2];} // Handle 'Tag' links
$res=rpc_search($peers, 'search/'.urlencode($_GET['q'].$filtertags).'/'.urlencode($sortby).'/'.$perpeer.'/'.($page*$perpage));
// Construct navigation links for pages
$pagefull=false;
foreach($res as $r){if(count($r)==$perpeer){$pagefull=true;}}
$pagenav='';
$getvariables='';
foreach($_GET as $k=>$v)
{
  if($k=='page'){continue;}
  $getvariables.=urlencode($k).'='.urlencode($v).'&';
}
if($page>1){$pagenav.='<a href="?'.$getvariables.'" class="pagenav">'._('First page').'</a>';}
if($page>0){$pagenav.='<a href="?'.$getvariables.'page='.($page-1).'" class="pagenav">'._('Previous page').'</a>';}
if($pagefull){$pagenav.='<a href="?'.$getvariables.'page='.($page+1).'" class="pagenav">'._('Next page').'</a>';}

// Flatten the results into a single array with peer information for each item
$results=Array();
foreach($res as $peer=>$res2)
{
  if(isset($res2['error']))
  {
    print('<div class="error">'.$peer.': '.$res2['error'].'</div>');
    continue;
  }
  foreach($res2 as $thing)
  {
    $thing['peer']=$peer;
    $results[]=$thing;
  }
}
function sortthings($a, $b) // Re-sort flattened results
{
  global $sortby;
  // TODO: More sorting options?
  switch($sortby)
  {
    case 'alpha': return ord($a['name'])-ord($b['name']);
    case 'old': return strtotime($a['date'])-strtotime($b['date']);
    case 'new':
    default: return strtotime($b['date'])-strtotime($a['date']);
  }
}
usort($results, 'sortthings');

// Construct the results page
$things='';
foreach($results as $thing)
{
  $by=htmlentities($thing['by']['name']);
  $by='<a href="'.BASEURL.'/user/'.$by.'@'.$thing['peer'].'" title="'.$by.'@'.$thing['peer'].'">'.htmlentities($thing['by']['displayname']).'</a>';
  $things.='<div class="thing"><div class="boxtop">By '.$by.'</div><a href="'.BASEURL.'/thing/'.$thing['id'].'@'.$thing['peer'].'/'.urlencode(str_replace(' ','_',$thing['name'])).'"><div class="boxbottom">'.htmlentities($thing['name']).'</div><img src="'.abslink($thing['peer'], $thing['preview']).'" /></a></div>';
}
?>
<div class="sidebar">
  <form action="<?=BASEURL?>/search">
    <input type="hidden" name="q" value="<?=htmlentities(isset($_GET['q'])?$_GET['q']:'')?>" />
    <?=_('Results per page (approximately):')?> <input type="number" name="perpage" value="<?=$perpage?>" /><br />
    <?=_('Sort by:')?> <select name="sort">
      <option value="new"<?=($sortby=='new'?' selected':'')?>><?=_('New')?></option>
      <option value="old"<?=($sortby=='old'?' selected':'')?>><?=_('Old')?></option>
      <option value="alpha"<?=($sortby=='alpha'?' selected':'')?>><?=_('Alphabetical')?></option>
    </select>
    <button><?=_('Update')?></button>
  </form>
  <p>
    <h4><?=_('Advanced search options')?></h4>
    <?=_('To search for something by tag:')?> <span class="code">tag:searchterm</span><br />
    <?=_('To search for something specifically in the name:')?> <span class="code">name:searchterm</span><br />
    <?=_('To search for something specifically in the description:')?> <span class="code">description:searchterm</span><br />
    <?=_('To negate a searchterm, skipping everything that matches:')?> <span class="code">-searchterm</span><br />
    <?=_('To match a specific phrase:')?> <span class="code">&quot;searchterm in multiple words&quot;</span><br />
  </p>
</div><br />
<?=$things?><br />
<?=$pagenav?>
