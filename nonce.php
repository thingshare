<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
function nonce()
{
  $num=($_SESSION['noncenum']++)%500; // Incremental suffix to avoid collisions
  $_SESSION['nonce'.$num]=bin2hex(random_bytes(16));
  return '<input type="hidden" name="nonce" value="'.$num.':'.$_SESSION['nonce'.$num]."\" />\n";
}
function checknonce()
{
  $nonce=explode(':', $_POST['nonce']);
  $res=(isset($_SESSION['nonce'.$nonce[0]]) && $_SESSION['nonce'.$nonce[0]]==$nonce[1]);
  if(!isset($_POST['asyncrequest']))
  {
    unset($_SESSION['nonce'.$nonce[0]]);
  }
  return $res;
}
?>
