<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('config.php');
if(isset($_COOKIE['PHPSESSID'])){session_start();}
if(!isset($_SESSION['id'])){header('Location: '.BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']));}
include_once('db.php');
include_once('nonce.php');
include_once('rpc.php');
$error='';
$info='';
// Resolve chain ID (and username of second party)
if(isset($path[2]) && $path[2]!='new' && $path[2]!='')
{
  $chain=mysqli_real_escape_string($db, $path[2]);
  $res=mysqli_query($db, 'select sender, recipient, subject from messages where user='.(int)$_SESSION['id'].' and chain="'.$chain.'" and latest');
  if(!($res=mysqli_fetch_row($res)))
  {
    $error='Message chain not found';
  }else{
    $toname=$res[($res[0]==$_SESSION['name'].'@'.DOMAIN)?1:0];
    $subject=$res[2];
  }
}else{
  $chain='';
  if(isset($_POST['to'])){$toname=$_POST['to'];}
  else if(isset($_GET['to'])){$toname=$_GET['to'];}
  else{$toname='';}
  $subject='';
}
// Check if we're blocking the user
$to_esc=mysqli_real_escape_string($db, $toname);
$res=mysqli_query($db, 'select user from userblocks where user='.(int)$_SESSION['id'].' and blocked="'.$to_esc.'" limit 1');
$blocked=mysqli_fetch_row($res);
// Send message
if($error=='' && ($path[2]!='new' || isset($_POST['to'])) && checknonce())
{
  $touser=explode('@', $toname);
  if(count($touser)!=2){$error=_('Invalid recipient');}
  else if(isset($_POST['blockuser'])) // Handle blocking and unblocking
  {
    if($_POST['blockuser']==1)
    {
      mysqli_query($db, 'insert into userblocks(user, blocked) values('.(int)$_SESSION['id'].', "'.$to_esc.'")');
      $info=sprintf(_('Blocked %s'), htmlentities($toname));
      $blocked=true;
    }else{
      mysqli_query($db, 'delete from userblocks where user='.(int)$_SESSION['id'].' and blocked="'.$to_esc.'"');
      $info=sprintf(_('Unblocked %s'), htmlentities($toname));
      $blocked=false;
    }
  }
  else if(!isset($_POST['msg']) || !isset($_POST['subject'])){$error=_('Missing message and/or subject');}
  else if($blocked){$error=_('Cannot send messages to blocked users');}else{
  // Store in DB
  $subject=$_POST['subject'];
  $from=mysqli_real_escape_string($db, $_SESSION['name'].'@'.DOMAIN);
  $timestamp=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
  $subject_esc=mysqli_real_escape_string($db, $subject);
  $msg=mysqli_real_escape_string($db, $_POST['msg']);
  $q='insert into messages(user, recipient, sender, sent, subject, message, msgread, latest'.(($chain=='')?'':', chain').') ';
  $q.='values('.(int)$_SESSION['id'].', "'.$to_esc.'", "'.$from.'", "'.$timestamp.'", "'.$subject_esc.'", "'.$msg.'", true, true'.(($chain=='')?'':', "'.$chain.'"').')';
  if(!mysqli_query($db, $q)){$error='Database error, message not sent';}else{
  $id=(int)mysqli_insert_id($db);
  if($chain=='') // Set chain ID for new chain
  {
    $path[2]=$id.'_'.DOMAIN;
    $chain=mysqli_real_escape_string($db, $path[2]);
    mysqli_query($db, 'update messages set chain="'.$chain.'" where id='.(int)$id);
  }
  // Send it to recipient's node
  $msg=Array('subject'=>$subject,
             'from'=>$_SESSION['name'],
             'message'=>$_POST['msg'],
             'chain'=>$path[2]);
  $data=rpc_post($touser[1], 'messages/'.$touser[0], $msg);
  if(isset($data['error']))
  {
    $error=$data['error'];
    // Delete the failed message from database
    mysqli_query($db, 'delete from messages where user='.(int)$_SESSION['id'].' and id='.$id);
  }else{
    $info=_('Message sent');
    // Update 'latest' on messages which are now old
    mysqli_query($db, 'update messages set latest=false where chain="'.$chain.'" and user='.(int)$_SESSION['id'].' and id!='.$id.' and sent<"'.$timestamp.'"');
  }
  }} // Error checks (and blocking)
}
include_once('head.php');
$messages='';
$header='';
// One view for overview, one view for thread/new
if(!isset($path[2]) || $path[2]=='') // Overview
{
  $header='<tr><th>'._('To/From').'</th><th>'._('Subject').'</th><th>'._('Date').'</th></tr>';
  $res=mysqli_query($db, 'select id, recipient, sender, sent, subject, message, msgread, chain from messages where user='.(int)$_SESSION['id'].' and latest order by sent desc');
  while($row=mysqli_fetch_assoc($res))
  {
    $user=(($row['recipient']==$_SESSION['name'].'@'.DOMAIN)?$row['sender']:$row['recipient']);
    $user='<a href="'.BASEURL.'/user/'.htmlentities($user).'" title="'.htmlentities($user).'">'.htmlentities(getdisplayname($user)).'</a>';
    $subjectline=htmlentities($row['subject']);
    $chain=htmlentities($row['chain']);
    $aclass=($row['msgread']?'':' class="highlight"'); // Highlight link if unread
    $messages.='<tr>';
    $messages.='  <td>'.$user.'</td>';
    $messages.='  <td><a href="'.BASEURL.'/messages/'.$chain.'"'.$aclass.'>'.$subjectline.'</a></td>';
    $messages.='  <td><span class="time" title="'.htmlentities($row['sent']).' UTC">'.timeago($row['sent']).'</span></td>';
    $messages.='</tr>';
  }
}
elseif($error=='' && $path[2]!='new') // Thread view
{
  include_once('parsedown/Parsedown.php');
  $md=new Parsedown();
  $res=mysqli_query($db, 'select id, recipient, sender, sent, subject, message, msgread from messages where user='.(int)$_SESSION['id'].' and chain="'.$chain.'" order by sent asc');
  while($row=mysqli_fetch_assoc($res))
  {
    $sender=htmlentities($row['sender']);
    $displayname=htmlentities(getdisplayname($row['sender']));
    $msg=$md->text(htmlentities($row['message']));
    $time=htmlentities($row['sent']);
    $timeago=timeago($row['sent']);
    $messages.='<div class="message'.($row['msgread']?'':' message_unread').'"><div class="message_sender"><a href="'.BASEURL.'/user/'.$sender.'" title="'.$sender.'">'.$displayname.'</a> <span class="time" title="'.$time.' UTC">'.$timeago.'</span></div>'.$msg.'</div>';
  }
  mysqli_query($db, 'update messages set msgread=true where user='.(int)$_SESSION['id'].' and chain="'.$chain.'" order by sent asc');
}
if($chain=='')
{
  $to='<label>'._('To:').' <input type="text" name="to" placeholder="user@node" value="'.htmlentities($toname).'" /></label><br />';
}else{
  $to=_('To:').' '.htmlentities($toname).'<br />';
}
if($error!=''){$info='<span class="error">'.$error.'</span>';}
?>
<h1><?=(($subject=='')?_('Messages'):htmlentities($subject))?></h1>
<?=$info?>
<table>
  <?=$header?>
  <?=$messages?>
</table>
<?php if(isset($path[2]) && $path[2]!=''){ ?>
<script src="<?=BASEURL?>/mdjs/mdjs.js"></script>
<?=(($path[2]=='new')?'':'<h2>'._('Reply').'</h2>')?>
<form method="post" action="<?=BASEURL?>/messages/<?=$path[2]?>">
  <p>
    <?=nonce().$to?>
    <?php if(!$blocked){ ?>
    <?=_('Subject:')?> <input type="text" name="subject" value="<?=$subject?>" /><br />
    <textarea rows="12" style="width:100%;" name="msg" onchange="document.getElementById('mdpreview').innerHTML='Markdown preview:<br />'+Mdjs.md2html(this.value.replace(/&/g,'&amp;amp;').replace(/</g,'&amp;lt;'));" onkeyup="this.onchange();"></textarea>
    <div id="mdpreview"></div>
    <button><?=_('Send')?></button>
    <?php } ?>
  </p>
  <?php if($toname!=''){
    print('<p><button name="blockuser" value="'.($blocked?'0':'1').'">'.sprintf($blocked?_('Unblock %s'):_('Block %s'), htmlentities($toname)).'</button></p>');
  }
  ?>
</form>
<?php } ?>
