<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
include_once('rpc.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_FEDERATION)){die(_('Insufficient privileges'));}

$error='';
if(checknonce()) // Save changes
{
  if(isset($_POST['options']))
  {
    setoption('autofollow', isset($_POST['autofollow']));
  }
  if(isset($_POST['addpeer']) || isset($_POST['blacklistpeer']))
  {
    $peer=mysqli_real_escape_string($db, $_POST['newpeer']);
    $blacklist=(isset($_POST['blacklistpeer'])?'1':'0');
    mysqli_query($db, 'delete from peers where domain="'.$peer.'"');
    mysqli_query($db, 'insert into peers(domain, blacklist) values("'.$peer.'", '.$blacklist.')');
  }
  if(isset($_POST['deletepeer']))
  {
    $peer=mysqli_real_escape_string($db, $_POST['deletepeer']);
    mysqli_query($db, 'delete from peers where domain="'.$peer.'"');
  }
  if(isset($_POST['action']) && $_POST['action']=='addindirectpeers')
  {
    $peers=Array();
    $blacklist=Array();
    $res=mysqli_query($db, 'select domain, blacklist from peers order by domain asc');
    while($row=mysqli_fetch_assoc($res))
    {
      if($row['blacklist']){$blacklist[]=$row['domain'];}else{$peers[]=$row['domain'];}
    }
    $oldpeers=count($peers); // For inserting new peers later
    for($i=0; $i<count($peers); ++$i)
    {
      $newpeers=rpc_get($peers[$i], 'peers');
      if(isset($newpeers['error']))
      {
        $error.=$peers[$i].': '.$newpeers['error'].'<br />';
        if($i>=$oldpeers){array_splice($peers, $i, 1); --$i;} // Don't add if there are issues (but ignore old peers because we're not autoremoving)
        continue;
      }
      foreach($newpeers['peers'] as $peer)
      {
        // Skip already known peers
        if($peer==DOMAIN || in_array($peer, $peers) || in_array($peer, $blacklist)){continue;}
        $peers[]=$peer;
      }
    }
    for($i=$oldpeers; $i<count($peers); ++$i)
    {
      $peer=mysqli_real_escape_string($db, $peers[$i]);
      mysqli_query($db, 'insert into peers(domain, blacklist) values("'.$peer.'", 0)');
    }
  }
  if(isset($_POST['addfilter']))
  {
    $filter=mysqli_real_escape_string($db, $_POST['newfilter']);
    mysqli_query($db, 'delete from filtertags where tag="'.$filter.'"');
    mysqli_query($db, 'insert into filtertags(tag) values("'.$filter.'")');
  }
  if(isset($_POST['deletefilter']))
  {
    $filter=mysqli_real_escape_string($db, $_POST['deletefilter']);
    mysqli_query($db, 'delete from filtertags where tag="'.$filter.'"');
  }
}
if($error!=''){$error='<span class="error">'.$error.'</span>';}

// Load current
$peers='';
$res=mysqli_query($db, 'select domain, blacklist from peers order by domain asc');
while($row=mysqli_fetch_assoc($res))
{
  $domain=htmlentities($row['domain']);
  if($row['blacklist']){$domain='<span class="blacklist">'.$domain.'</span>';}
  $peers.=$domain.'<button name="deletepeer" value="'.htmlentities($row['domain']).'">X</button><br />';
}
$filters='';
$res=mysqli_query($db, 'select tag from filtertags');
while($row=mysqli_fetch_assoc($res))
{
  $tag=htmlentities($row['tag']);
  $filters.='<span class="blacklist">'.$tag.'</span><button name="deletefilter" value="'.$tag.'">X</button><br />';
}
$autofollowcheck=(getoption('autofollow')?' checked':'');
?>
<?=$error?>
<form method="post"><?=nonce()?>
  <h2><?=_('Options')?></h2>
  <label><input type="checkbox" name="autofollow"<?=$autofollowcheck?> />Automatically follow peers</label><br />
  <input type="submit" name="options" value="<?=_('Save')?>" />
</form>
<form method="post"><?=nonce()?>
  <h2><?=_('Peers')?></h2>
  <?=$peers?>
</form>
<form method="post"><?=nonce()?>
  <input type="text" name="newpeer" placeholder="<?=_('domain.tld')?>" /><button name="addpeer"><?=_('Add peer')?></button><button name="blacklistpeer" title="<?=_('Don\'t autofollow this peer')?>"><?=_('Add peer to blacklist')?></button><br />
  <button name="action" value="addindirectpeers"><?=_('Add indirect peers')?></button>
</form>
<form method="post"><?=nonce()?>
  <h2><?=_('Filters')?></h2>
  <?=_('Peers\' things with these tags will not be shown in search results on this instance')?><br />
  <?=$filters?>
</form>
<form method="post"><?=nonce()?>
  <input type="text" name="newfilter" placeholder="<?=_('Tag to filter')?>" /><button name="addfilter"><?=_('Add filter')?></button><br />
</form>
