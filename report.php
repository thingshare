<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('head.php');
// Only accept reports from logged in users
if(!isset($_SESSION['id'])){header('Location: '.BASEURL.'/login?returnto='.urlencode($_SERVER['REQUEST_URI']));}
include_once('nonce.php');
include_once('rpc.php');
$target=$path[2];
for($i=3; $i<count($path); ++$i){$target.='/'.$path[$i];}
$item=explode('@',$target);
$target=$item[0];
if(checknonce() && isset($_POST['reason']))
{
  $reason=$_POST['reason'];
  if($reason=='other' && isset($_POST['reason_other'])){$reason.=': '.$_POST['reason_other'];}
  $obj=Array(
    'user'=>$_SESSION['name'],
    'reason'=>$reason,
    'target'=>$target);
  $res=rpc_post($item[1], 'report', $obj);
  if(isset($res['status']) && strtoupper($res['status'])=='OK')
  {
    print('<h1>'._('Report received').'</h1>');
    include_once('foot.php');
    exit();
  }
}

// Get the name of the target
$obj=rpc_get($item[1], 'thing/'.explode('@',$path[3])[0]);
$targetname=htmlentities($target.'@'.$item[1].' ('.($path[2]=='thing'?'':'on ').$obj['name'].')');
?>
<form method="post">
  <?=nonce()?>
  <h1><?=_('Report')?></h1>
  <p><?=$targetname?></p>
<!--
TODO: Mod-defined canned reports? Local or target's node?
target's node makes sense from the perspective of adding canned reports based on what people fill in as "other". E.g. if a node is dealing with a problem specific to that node they'll see that a new canned report may be needed. And the local node wouldn't see it unless it faces the same problem.
So target's it is
-->
  <label><input type="radio" name="reason" value="other" id="report_other" />Other: <input type="text" name="reason_other" onfocus="document.getElementById('report_other').checked=true;" /></label><br />
  <button>Report</button>
</form>
