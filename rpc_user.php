<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(!isset($path[3])){die();}
include_once('db.php');
include_once('files.php');
$user=mysqli_real_escape_string($db, urldecode($path[3]));
$res=mysqli_query($db, 'select id, displayname, profile, status from users where name="'.$user.'"');
$res=mysqli_fetch_assoc($res);
if(!$res){header('HTTP/1.1 404 Not found'); die('{"error":"User not found"}');}
$id=(int)$res['id'];
$obj=Array();
// Populate user info
$obj['displayname']=$res['displayname'];
$obj['profile']=$res['profile'];
$obj['things']=Array();
if($res['status']==ACCOUNT_BANNED){$obj['banned']=true;}
// Gather things
$res=mysqli_query($db, 'select id, thingid, name, description, posted from things where user='.$id.' and latest and !removed order by posted desc');
while($row=mysqli_fetch_assoc($res))
{
  $thing=Array('id'=>$row['thingid'],
               'name'=>$row['name'],
               'description'=>$row['description'],
               'date'=>$row['posted']);
  // Grab preview from the chosen file, or the first file if none is chosen for preview
  $res2=mysqli_query($db, 'select name, hash from files where thing='.(int)$row['id'].' order by preview desc limit 1');
  $row=mysqli_fetch_assoc($res2);
  $thing['preview']=getpreview($row['name'], $row['hash']);
  $obj['things'][]=$thing;
}
print(json_encode($obj));
?>
