/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    See /COPYING for license text (AGPLv3+)
*/
function threedview(link,w,h)
{
  div=link.parentElement.parentElement;
  // Toggle view
  var x3d=div.getElementsByTagName('x3d');
  if(x3d.length>0)
  {
    link.textContent='3D view';
    // Remove 3D view
    for(var i=0; i<x3d.length; ++i){x3d[i].remove();}
    // Restore 2D view
    var img=div.getElementsByTagName('img');
    for(var i=0; i<img.length; ++i){img[i].style.display='';}
    return false;
  }else{
    link.textContent='2D view';
  }
  // Hide 2D view
  var img=div.getElementsByTagName('img');
  for(var i=0; i<img.length; ++i){img[i].style.display='none';}
  // Add 3D preview (tried to do this with the DOM first, but x3dom wouldn't pick it up)
  div.innerHTML+='<x3d width="'+w+'px" height="'+h+'px"><scene><inline url="'+link.dataset.file+'"></inline></scene></x3d>';
  x3dom.reload(); // Load the new addition
  return false;
}
