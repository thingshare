<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('db.php');
include_once('nonce.php');
if(!isset($_SESSION['id'])){die(_('Insufficient privileges'));}
if(!($privileges&PRIV_LICENSES)){die(_('Insufficient privileges'));}
if(checknonce()) // Save changes
{
  if(isset($_POST['action']) && $_POST['action']=='newlicense')
  {
    $name=mysqli_real_escape_string($db, $_POST['newname']);
    $simple=mysqli_real_escape_string($db, $_POST['newsimple']);
    $full=mysqli_real_escape_string($db, $_POST['newfull']);
    if(!mysqli_query($db, 'insert into licenses(name, simple, full, removed, defaultlicense) values("'.$name.'", "'.$simple.'", "'.$full.'", false, false)'))
    {
      mysqli_query($db, 'update licenses set simple="'.$simple.'", full="'.$full.'", removed=false, defaultlicense=false where name="'.$name.'" and removed');
    }
  }
  if(isset($_POST['removelicense']))
  {
    $name=mysqli_real_escape_string($db, $_POST['removelicense']);
    mysqli_query($db, 'update licenses set removed=true where name="'.$name.'"');
  }
  if(isset($_POST['defaultlicense']))
  {
    $name=mysqli_real_escape_string($db, $_POST['defaultlicense']);
    mysqli_query($db, 'update licenses set defaultlicense=false where name!="'.$name.'"');
    mysqli_query($db, 'update licenses set defaultlicense=true where name="'.$name.'"');
  }
}

// Load current
$licenses='';
$res=mysqli_query($db, 'select name, simple, full, defaultlicense from licenses where !removed order by name asc');
while($row=mysqli_fetch_assoc($res))
{
  $name=htmlentities($row['name']);
  $simple=htmlentities($row['simple']);
  $full=htmlentities($row['full']);
  $licenses.='<tr>';
  $licenses.='  <td>'.$name.'</td>';
  $licenses.='  <td><div class="scrolltext">'.$simple.'</div></td>';
  $licenses.='  <td><div class="scrolltext">'.$full.'</div></td>';
  $licenses.='  <td>';
  $licenses.='    <button name="removelicense" value="'.$name.'">X</button>';
  if(!$row['defaultlicense'])
  {
    $licenses.='  <button name="defaultlicense" value="'.$name.'">'._('Make default').'</button>';
  }
  $licenses.='  </td>';
  $licenses.='</tr>';
}
?>
<h1><?=_('Licenses')?></h1>
<form method="post"><?=nonce()?>
  <table>
    <tr>
      <th><?=_('Name')?></th>
      <th><?=_('Simple')?></th>
      <th><?=_('Full (or link to full)')?></th>
      <th></th>
    </tr>
    <?=$licenses?>
  </table>
</form>
<form method="post"><?=nonce()?>
  <?=_('Name')?>: <input type="text" name="newname" /><br />
  <?=_('Simplified terms')?>:<br />
  <textarea name="newsimple"></textarea><br />
  <?=_('Full license text (or link)')?>:<br />
  <textarea name="newfull"></textarea><br />
  <button name="action" value="newlicense"><?=_('Add')?></button>
</form>
