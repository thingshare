  </div>
  <div class="footer">
    <div class="c2 top">
      <h4>About</h4>
      Thingshare is a federated system for sharing data for home manufacturing. For more information see <a href="https://thingshare.ion.nu/">thingshare.ion.nu</a>
    </div><div class="c2 top">
      <h4>Contact</h4>
      E-mail address: <a href="mailto:<?=EMAIL?>"><?=str_replace('@','@<wbr/>',EMAIL)?></a>
    </div>
    <div class="footnote">
       Content displayed on Thingshare is property of respective creator, not of Thingshare or its operators
    </div>
  </div>
</body>
</html>
