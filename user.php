<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
include_once('head.php');
include_once('db.php');
include_once('rpc.php');
$user=explode('@',$path[2]);
$userobj=rpc_get($user[1], 'user/'.$user[0]);
if(isset($userobj['error']))
{
  print('<div class="error">'.$userobj['error'].'</div>');
  include_once('foot.php');
  exit();
}
$displayname=htmlentities($userobj['displayname']);
$username=htmlentities(urldecode(implode('@',$user)));
include_once('parsedown/Parsedown.php');
$md=new Parsedown();
$profile=$md->text(htmlentities($userobj['profile']));
$things='';
if($user[1]==DOMAIN && urldecode($user[0])==$_SESSION['name'])
{
  $profile.='<a href="'.BASEURL.'/editprofile">'._('Edit').'</a>';
  $things='<div class="thing thing-add"><a href="'.BASEURL.'/editthing/new" title="New thing">+</a></div>';
}
if(isset($userobj['banned']) && $userobj['banned'])
{
  $unban='';
  if($user[1]==DOMAIN && $privileges&PRIV_MODERATE)
  {
    include_once('nonce.php');
    $unban='<form method="post" action="'.BASEURL.'/admin/moderate">'.nonce().'<button name="unban" value="'.htmlentities($user[0]).'">'._('Unban').'</button></form>';
  }
  $profile=_('This user has been banned.').$unban.'<br />'.$profile;
}
// Iterate through 'things'
foreach($userobj['things'] as $thing)
{
  $things.='<div class="thing"><a href="'.BASEURL.'/thing/'.$thing['id'].'@'.$user[1].'/'.urlencode(str_replace(' ','_',$thing['name'])).'"><div class="boxbottom">'.htmlentities($thing['name']).'</div><img src="https://'.$user[1].$thing['preview'].'" /></a></div>';
}
// TODO: Profile picture?
?>
<h1><?=$displayname?> <small class="subheader"><?=$username?></small></h1>
<a href="<?=BASEURL?>/messages/new?to=<?=$username?>"><?=_('Send message')?></a>
<?=$profile?><br />
<?=$things?>
