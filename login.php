<?php
/*
    This file is part of Thingshare, a federated system for sharing data for home manufacturing (e.g. 3D models to 3D print)
    https://thingshare.ion.nu/
    Copyright (C) 2020-2021  Alicia <alicia@ion.nu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
if(isset($_POST['user']) && isset($_POST['pass']))
{
  include_once('db.php');
  // Check for bruteforcing attempts
  $fail_time=getoption('loginfail_time', 3600);
  $fail_limit=getoption('loginfail_limit', 10);
  $fail_lockout=getoption('loginfail_lockout', 3600*24);
  $ip=mysqli_real_escape_string($db, $_SERVER['REMOTE_ADDR']);
  $oldtime=mysqli_real_escape_string($db, date('Y-m-d H:i:s', time()-$fail_time));
  mysqli_query($db, 'delete from loginfails where timestamp<"'.$oldtime.'"');
  $res=mysqli_query($db, 'select count(*) from loginfails where ip="'.$ip.'"');
  $res=mysqli_fetch_row($res);
  $fail_count=$res[0];
  if($fail_count>$fail_limit)
  {
    $error=_('Login attempt limit exceeded');
  }else{
    // Check password
    $error=_('Incorrect username/password');
    $user=mysqli_real_escape_string($db, $_POST['user']);
    $res=mysqli_query($db, 'select salt, password, id, status, displayname from users where name="'.$user.'"');
    if($res=mysqli_fetch_assoc($res))
    {
      $hash=explode(':', $res['password']);
      $pass=hash($hash[0], $_POST['pass'].$res['salt']);
      if($pass==$hash[1])
      {
        switch($res['status'])
        {
          case ACCOUNT_ACTIVE:
            session_start();
            $_SESSION['name']=$_POST['user'];
            $_SESSION['id']=$res['id'];
            $_SESSION['displayname']=$res['displayname'];
            header('Location: '.(isset($_GET['returnto'])?urldecode($_GET['returnto']):BASEURL.'/'));
            exit();
          case ACCOUNT_BANNED: $error=_('Banned'); break;
          case ACCOUNT_EMAILUNVERIFIED: $error=_('Please check for a verification e-mail'); break;
        }
      }
    }
    $time=mysqli_real_escape_string($db, date('Y-m-d H:i:s'));
    mysqli_query($db, 'insert into loginfails(ip, timestamp) values("'.$ip.'", "'.$time.'")');
    if($fail_count==$fail_limit) // Limit reached, apply lockout
    {
      $time=mysqli_real_escape_string($db, date('Y-m-d H:i:s', time()-$fail_time+$fail_lockout));
      mysqli_query($db, 'update loginfails set timestamp="'.$time.'" where ip="'.$ip.'"');
    }
  }
  $error='<div class="error">'.$error.'</div>';
}else{
  $error='';
}
include_once('head.php');
?>
<form method="post" id="loginform">
  <center>
    <?=$error?>
    <input type="text" name="user" placeholder="<?=_('Username')?>" /><br />
    <input type="password" name="pass" placeholder="<?=_('Password')?>" /><br />
    <button><?=_('Log in')?></button>
  </center>
<form>
